﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLBankSetting
    {
        private string bankTID;
        private string bankMID;
        private string bankName;
        private string currency;
        private string address;
        private string country;
        private string bankLogo;
        private string bankLogoForPrint;
        private int bLogo;
        private string bLForPrint;
        private string bankCode;

        public CLBankSetting()
        {

        }

        public CLBankSetting(string banktid, string bankmid, string bankname, string currency, string address,
                             string country, string banklogo, string banklogoforprint, int logo, string bLForPrint, string bankcode)
        {
            this.bankTID = banktid;
            this.bankMID = bankmid;
            this.bankName = bankname;
            this.currency = currency;
            this.address = address;
            this.country = country;
            this.bankLogo = banklogo;
            this.bankLogoForPrint = banklogoforprint;
            this.bLogo = logo;
            this.bLForPrint = bLForPrint;
            this.bankCode = bankcode;
        }

        public String getBankTID()
        {
            return bankTID;
        }

        public void setBankTID(String bankTID)
        {
            this.bankTID = bankTID;
        }

        public String getBankMID()
        {
            return bankMID;
        }

        public void setBankMID(String bankMID)
        {
            this.bankMID = bankMID;
        }

        public String getBankName()
        {
            return bankName;
        }

        public void setBankName(String bankName)
        {
            this.bankName = bankName;
        }

        public String getCurrency()
        {
            return currency;
        }

        public void setCurrency(String currency)
        {
            this.currency = currency;
        }

        public String getAddress()
        {
            return address;
        }

        public void setAddress(String address)
        {
            this.address = address;
        }

        public String getCountry()
        {
            return country;
        }

        public void setCountry(String country)
        {
            this.country = country;
        }

        public String getBankLogo()
        {
            return bankLogo;
        }

        public void setBankLogo(String bankLogo)
        {
            this.bankLogo = bankLogo;
        }

        public String getBankLogoForPrint()
        {
            return bankLogoForPrint;
        }

        public void setBankLogoForPrint(String bankLogoForPrint)
        {
            this.bankLogoForPrint = bankLogoForPrint;
        }

        public int getbLogo()
        {
            return bLogo;
        }

        public void setbLogo(int bLogo)
        {
            this.bLogo = bLogo;
        }

        public String getbLForPrint()
        {
            return bLForPrint;
        }

        public void setbLForPrint(String bLForPrint)
        {
            this.bLForPrint = bLForPrint;
        }

        public String getBankCode()
        {
            return bankCode;
        }

        public void setBankCode(String bankCode)
        {
            this.bankCode = bankCode;
        }


    }
}

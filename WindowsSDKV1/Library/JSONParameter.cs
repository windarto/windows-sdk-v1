﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class ParamList {
        public Dictionary<String, String> paramlists = new Dictionary<String, String> {
            {"100","EXPIRY_L1"},
            {"101","EXPIRY_L2"},
            {"120","SHOP_NAME"},
            {"121","WALK_IN_MID"}
        };

        public String code;

        ParamList(String code) {
            this.code = code;
        }
    }

    public class JSONParameter
    {
        private String paramCode;
        private String paramName;
        private String paramValue;

        public JSONParameter(ParamList param, String value) {
            this.paramCode = param.code;
            this.paramName = param.paramlists[param.code];
            this.paramValue = value;
        }

        public String getParamCode() {
            return this.paramCode;
        }

        public String getParamName() {
            return this.paramName;
        }

        public String getParamValue() {
            return this.paramValue;
        }
    }
}

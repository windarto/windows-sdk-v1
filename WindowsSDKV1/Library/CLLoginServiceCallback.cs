﻿using System;
using System.Security.Cryptography;

namespace WindowsSDKV1.Library
{
    public interface CLLoginServiceCallback
    {
        void onHandshakeSuccess(String result);

        void onHandshakeFail(int httpStatusCode);

        void onDecryptError();

        void onLoginServiceStartActivation(JSONServiceDTO content);

        void onLoginServiceSuccess(JSONServiceDTO result, CLLoginResponse clLoginResponse);

        void onLoginServiceRequestToken(CLJsonHttpResult<JSONServiceDTO> result);

        void onLoginServiceNotOk();

        void onGetPublicKey(JSONServiceDTO response, RSAParameters serverPublicKey);

        void doLogin();

        void onLoginServiceError(JSONServiceError clErrorResponse);
    }
}

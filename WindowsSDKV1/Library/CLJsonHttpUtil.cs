﻿using System;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace WindowsSDKV1.Library
{
    public class CLJsonHttpUtil : ICLJsonHttpUtil
    {
        private HttpWebRequest connection;

        public HttpWebRequest getConnectionInstance(String Type, String requestMethod) {
            if (requestMethod.Equals("POST"))
            {
                return CLReusableHttpClient.postHttpInstance(Type);
            }
            else {
                return CLReusableHttpClient.getHttpInstance(Type);
            }
        }

        public void postAsObjectAsync(string session, JSONServiceDTO dto, CLKeyJCE keyJCE, HttpWebRequest webRequest, CLJsonHttpResult<JSONServiceDTO> resultout)
        {
            throw new NotImplementedException();
        }

        public void postAsStringAsync(String Encrypted, CLJsonHttpResult<String> result)
        {
            StreamWriter stream = new StreamWriter(connection.GetRequestStream());
            try
            {
                stream.Write(Encrypted);
                stream.Flush();
                stream.Close();

                try 
                {
                    var responweb = (HttpWebResponse)connection.GetResponse();
                    StreamReader streamout = new StreamReader(responweb.GetResponseStream());
                    String capture = streamout.ReadToEnd();
                    result.setContent(capture);
                    result.setHttpStatusCode(200);
                }
                catch (IOException e) {
                    Debug.WriteLine(e.Message);
                }
            }
            catch (IOException e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class Application
    {
        readonly private String description;
        readonly private String AID;
        readonly private String versions;
        readonly private String TACDenial;
        readonly private String TACOnline;
        readonly private String TACDefault;

        public Application(string description, string aid, string versions, string tacdenial, string taconline, string tacdefault) : 
            base(){
            this.description = description;
            this.AID = aid;
            this.versions = versions;
            this.TACDenial = tacdenial;
            this.TACOnline = taconline;
            this.TACDefault = tacdefault;
        }

        public string getDescription() {
            return this.description;
        }

        public string getAID()
        {
            return this.AID;
        }

        public string getVersions()
        {
            return this.versions;
        }

        public string getTACDenial()
        {
            return this.TACDenial;
        }

        public string getTACOnline() {
            return this.TACOnline;
        }

        public string getTACDefault()
        {
            return this.TACDefault;
        }
    }
}

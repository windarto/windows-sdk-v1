﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLMathUtil
    {
        CLMathUtil() {

        }
        public static String byteArraytoString(byte[] b)
        {
            if ((b == null) || (b.Length == 0))
            {
                return "";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                if ((b != null) && (b.Length > 0))
                {
                    int temp = (b[0] & 240) >> 4;
                    sb.Append(temp.ToString("X"));
                    int temp2 = (b[0] & 15);
                    sb.Append(temp2.ToString("X"));
                }

                for (int i = 1; i < b.Length; i++)
                {
                    int temp3 = (b[i] & 240) >> 4;
                    sb.Append(temp3.ToString("X"));
                    int temp4 = (b[i] & 15);
                    sb.Append(temp4.ToString("X"));
                }
                return sb.ToString().ToUpper();
            }
        }

        public static byte[] stringToByteArray(String s)
        {

            s.Replace(" ", "");
            int leng = s.Length;
            byte[] data = new byte[leng / 2];
            int basenumber = 16;
            for (int i = 0; i < leng; i += 2)
            {
                int temp1 = (Convert.ToInt32(s[i].ToString(), basenumber) << 4);
                int temp2 = Convert.ToInt32(s[i + 1].ToString(), basenumber);
                data[i / 2] = (byte)(temp1 + temp2);
            }
            return data;

        }
    }
}

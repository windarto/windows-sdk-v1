﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public interface ICLHandshakeModel
    {
        void doPostEncrypted(ICLJsonHttpUtil jsonHttpUtil, String Encrypted);

        void decryptKey(String decryptededAesKey, String aesEncryptedMsg);
    }
}

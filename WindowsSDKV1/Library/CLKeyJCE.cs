﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;


namespace WindowsSDKV1.Library { 
    public class CLKeyJCE
    {
        private string accessKey;
        private string secretKey;
        private string sessionToken;
        private RSACryptoServiceProvider KeyPair;
        private RSAParameters rsaPublicKey;
        private string clientID;
        private RSAParameters clientPrivateKey;
        private string mobileuserID;
        private string aggregatorID;

        public string getAccessKey()
        {
            return this.accessKey;
        }

        public void setAccessKey(string accesskey)
        {
            this.accessKey= accesskey;
        }

        public string getSecretKey()
        {
            return this.secretKey;
        }

        public void setSecretKey(string secretkey)
        {
            this.secretKey = secretkey;
        }

        public string getSessionToken()
        {
            return this.sessionToken;
        }

        public void setSessionToken(string sessiontoken)
        {
            this.sessionToken = sessiontoken;
        }

     public RSACryptoServiceProvider getKeyPair()
     {
         return this.KeyPair;
     }

     public void setKeyPair(RSACryptoServiceProvider keypair)
     {
         this.KeyPair= keypair;
     }

     public RSAParameters getRSAPublicKey()
     {
         return this.KeyPair.ExportParameters(false);
     }

     public void setRSAPublicKey(RSAParameters rsapublickey)
     {
         this.rsaPublicKey= rsapublickey;
     }

    public string getClientID()
        {
            return this.clientID;
        }

        public void setClientID(string clientid)
        {
            this.clientID= clientid;
        }

        public RSAParameters getClientPrivatePublicKey()
        {
            return this.KeyPair.ExportParameters(true);
        }

        public void setClientPrivatePublicKey(RSAParameters clientprivatepublickey)
        {
            this.clientPrivateKey = clientprivatepublickey;
        }
        
        public string getMobileUserID()
        {
            return this.mobileuserID;
        }

        public void setMobileUserID(string mobileuserid)
        {
            this.mobileuserID= mobileuserid;
        }

        public string getAggregatorID() {
            return this.aggregatorID;
        }  

        public void setAggregatorID(string aggregatorid)
        {
            this.aggregatorID = aggregatorid;
        }
    }
}
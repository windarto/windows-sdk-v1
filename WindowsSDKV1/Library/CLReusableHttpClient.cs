﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    class CLReusableHttpClient
    {
        private static readonly int CONN_TIMEOUT = 15000;
        private static readonly int SO_TIMEOUT = 120000;
        private static readonly String MAIN_URL = "http://staging.cashlez.com:9080/pgw/api/v1";
        //        private static readonly String mainurl = "https://secure.cashlez.com:9080/pgw/api/v1";

        public static HttpWebRequest postHttpInstance(String type) {
            var httpweb = (HttpWebRequest)WebRequest.Create(MAIN_URL+"/"+type);
            httpweb.Timeout = CONN_TIMEOUT;
            httpweb.ReadWriteTimeout = SO_TIMEOUT;
            httpweb.Method = "POST";
            httpweb.ContentType = "application/json";
            return httpweb;
        }

        public static HttpWebRequest getHttpInstance(String type)
        {
            var httpweb = (HttpWebRequest)WebRequest.Create(MAIN_URL + "/" + type);
            httpweb.Timeout = CONN_TIMEOUT;
            httpweb.ReadWriteTimeout = SO_TIMEOUT;
            httpweb.Method = "GET";
            httpweb.ContentType = "application/json";
            return httpweb;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLErrorStatusValue
    {
        readonly private Dictionary<String, String> ErrorMessage = new Dictionary<String, String> {
            {"app_name","Cashlez Library"} ,
            {"failure_response","Process failed, please try again."},
            {"unauthorized_response","Your session is revoked, please do login again."},
            {"activation_validation","Please fill activation code."},
            {"no_network_info","Check your connection."},
            {"session_not_valid_info","Please do login first."},
            {"handshake_failed_info","Handshake process failed."},
            {"login_not_ok_info","Login process failed."},
            {"login_success_info","Login success."},
            {"payment_status_info","Your payment is"},
            {"reversal_before_void_success_info","Reversal before void success."},
            { "reversal_void_success_info" ," Reversal void success."},
            { "send_receipt_success_info" ," Receipt sent."},
            { "get_history_success_info" ," Get history success."},
            { "get_history__detail_success_info" ," Get transaction history detail success."},
            { "activation_success_info" ," Activation success."},
            { "check_reader_validation" ," Please provide valid reader companion."},
            { "initialization_error_message" ," Initialization error."},
            { "initialization_error_key" ," error.initialization - fault "},
            { "session_expired_message" ," Session expired."},
            { "session_expired_key" ," error.session - timed -out"},
            { "tle_ltwk_key_download_error_message" ," TLE LTWK key download error."},
            { "tle_ltwk_key_download_error_key" ," error.tle - ltwk - download - error "},
            { "tle_logon_download_error_message" ," TLE Logon download error."},
            { "tle_logon_download_error_key" ," error.tle - logon - error "},
            { "page_number_is_invalid_message" ," Page number is invalid."},
            { "page_number_is_invalid_key" ," error.merchant.invalid - pagination "},
            { "exceed_three_attemps_message" ," You have exceeded a maximum number of three (3) attempts.Please contact your Merchant System Administrator."},
            { "exceed_three_attemps_key" ," error.AccountLocked.3times"},
            { "reader_not_link_message" ," Reader is not linked to the current merchant."},
            { "reader_not_link_key" ," error.device.merchant - device - mismatch "},

            { "application_expired_message" ," Application Expired, please update the application."},
            { "application_expired_key" ," error.application.expired "},

            { "new_application_available_message" ," New version is available, please update the application."},
            { "new_application_available_key" ," error.application.newversion "},

            { "user_is_not_active_message" ," User is not active."},
            { "user_is_not_active_key" ," error.UserInactive "},

            { "time_out_message" ," Connection timed out."},
            { "time_out_key" ," error.connection - timed -out"},
            { "transaction_already_reversed_message" ," Transaction is already reversed."},
            { "transaction_already_reversed_key" ," error.trxReversed "},

            { "not_authorized_user_message" ," You are not authorised to void or settle transactions."},
            { "not_authorized_user_key" ," error.voidPermissioDenied "},

            { "exceed_five_attemps_message" ," You have exceeded a maximum number of five (5) attempts.Please contact your Merchant System Administrator."},
            { "exceed_five_attemps_key" ," error.AccountLocked.5times"},
            { "device_not_unique_message" ," Please activate account using another phone /device."},
            { "device_not_unique_key" ," error.phone.notMatch "},
            {"device_not_belong_to_bank_message","Invalid Reader."},
            { "device_not_belong_to_bank_key" ," error.reader.notBelongsToBank "},

            { "please_use_same_smart_reader_message" ," Please use the same Smart Reader."},
            { "please_use_same_smart_reader_key" ," error.phone.notMatch "},

            { "invalid_phone_id_message" ," Invalid phone ID. Please reset your Smart Reader."},
            { "invalid_phone_id_key" ," error.phone.notMatch "},

            { "reader_is_inactive_or_suspended_message" ," Reader is inactive or suspended. Please insert another reader."},
            { "reader_is_inactive_or_suspended_key" ," error.cardReaderSuspended "},

            { "reader_mulfunction_message" ," Reader malfunction.Please contact our Merchant Hotline for replacement."},
            { "reader_mulfunction_key" ," error.reader.not - provisioned "},

            { "tid_is_suspended_or_not_linked_message" ," TID is suspended or not linked to Mobile User."},
            { "tid_is_suspended_or_not_linked_key" ," error.tid.deactivated "},

            { "no_tid_is_linked_message" ," No TID is linked with this mobile user."},
            { "no_tid_is_linked_key" ," error.login.tidUnavailable " },
 
            { "invalid_login_message" ," Invalid login, please try again or contact your Merchant System Administrator." },
            { "invalid_login_key" ," error.merchant.id - pin - mismatch " },
 
            { "user_pin_has_to_be_a_6_message" ," User PIN has to be a 6 numeric characters." },
            { "user_pin_has_to_be_a_6_key" ," error.PIN.length "},

            { "please_do_not_reuse_the_last_5_passwor_message" ," Please do not reuse the last 5 password."},
            { "please_do_not_reuse_the_last_5_passwor_key" ," error.mobileUser.ReuseOldPassword "},

            { "invalid_activation_code_message" ," Invalid activation code. Please try again."},
            { "invalid_activation_code_key" ," error.mobileUser - activation - mismatch "},

            { "please_ensure_user_id_and_pin_are_valid_message" ," Please ensure User ID and User PIN are valid. This will be your last attempt before your account is suspended."},
            { "please_ensure_user_id_and_pin_are_valid_key" ," error.mobileUser.finalAttempt "},

            { "activation_failed_message" ," Activation failed."},
            { "activation_failed_key" ," error.ActivationFailed "},

            { "you_are_using_an_outdated_application_message" ," You are using an outdated application.Please update your version."},
            { "you_are_using_an_outdated_application_key" ," update.mobile.please "},

            { "unable_to_find_resource_message" ," Unable to find resource you\'re looking for."},
            { "unable_to_find_resource_key" ," error.system.resource "},

            { "password_must_has_6_numbers_message" ," Password must has 6 numbers."},
            { "password_must_has_6_numbers_key" ," error.password.invalidFormat "},

            { "old_password_must_be_different_with_new_password_message" ," Old password must be different with new password."},
            { "old_password_must_be_different_with_new_password_key" ," error.password.samePassword "},
            {"new_password_already_used_message","New password already used before"},
            { "new_password_already_used_key" ," error.password.usedPassword "},
            {"wrong_password_when_voiding_message","Wrong password when voiding."},
            { "wrong_password_when_voiding_key" ," error.void.unauthorized"},
            { "you_are_not_authorised_to_void_message" ," You are not authorised to void transactions."},
            { "you_are_not_authorised_to_void_key" ," error.voidPermissioDenied "},

            { "void_failed_because_this_user_is_suspended_message" ," Void failed because this user is suspended."},
            { "void_failed_because_this_user_is_suspended_key" ," error.void.userSuspended"},
            { "settlement_failed_because_this_user_is_suspended_message" ," Settlement failed because this user is suspended."},
            { "settlement_failed_because_this_user_is_suspended_key" ," error.settlement.userSuspended "},

            { "invalid_format_user_login_message" ," Invalid format user login.User login can contains alphanumeric, \'.\' (dot), \'-\' (dash), \'_\'(underscore)."},
            { "invalid_format_user_login_key" ," error.user.invalidFormat "},

            { "wrong_password_when_settlement_message" ," Wrong password when settlement."},
            { "wrong_password_when_settlement_key" ," error.settlement.unauthorized "},

            { "you_are_not_authorised_to_settle_message" ," You are not authorised to settle this batch."},
            { "you_are_not_authorised_to_settle_key" ," error.settlement.PermissioDenied "},

            { "failed_to_do_settlement_message" ," Failed to do settlement, kindly contact our Merchant Hotline."},
            { "failed_to_do_settlement_key" ," error.settlement.failed "},

            { "unable_to_find_transaction_message" ," Unable to find transaction you\'re looking for."},
            { "unable_to_find_transaction_key" ," error.system.transaction "},

            { "connection_error_message" ," Connection Error.Please try again, if the problem persists kindly contact our Merchant Hotline."},
            { "connection_error_key" ," error.unableToContinueTransaction "},

            { "you_have_exceeded_your_daily_limit_message" ," You have exceeded your daily transaction limit. Please contact our Merchant Hotline."},
            { "you_have_exceeded_your_daily_limit_key" ," error.dailyTransactionLimit "},

            { "you_have_exceeded_your_monthly_limit_message" ," You have exceeded your monthly transaction limit. Please contact our Merchant Hotline."},
            { "you_have_exceeded_your_monthly_limit_key" ," error.monthlyTransactionLimit "},

            { "you_have_exceeded_your_transaction_limit_message" ," You have exceeded your transaction limit.Please contact our Merchant Hotline."},
            { "you_have_exceeded_your_transaction_limit_key" ," error.perTransactionLimit "},

            { "please_verify_mobile_number_message" ," Please verify mobile number."},
            { "please_verify_mobile_number_key" ," error.invalidphoneNumber "},

            { "please_verify_email_message" ," Please verify email."},
            { "please_verify_email_key" ," error.invalidEmailAddress "},

            { "email_or_sms_service_is_currently_unavailable_message" ," Email or SMS service is currently unavailable. Please contact Merchant Hotline."},
            { "email_or_sms_service_is_currently_unavailable_key" ," notification.service.down "},

            { "your_transaction_is_not_allowed_by_risk_management_message" ," Your transaction is not allowed by risk management.Please contact our Merchant Hotline."},
            { "your_transaction_is_not_allowed_by_risk_management_key" ," error.perTransactionLimit "},

            { "unable_to_process_payment_message" ," Unable to process payment.Host keys not properly configured."},
            { "unable_to_process_payment_key" ," error.keysNotProperlyConfigured "},

            { "invalid_template_sms_message" ," Invalid template SMS."},
            { "invalid_template_sms_key" ," error.invalidTemplateSms "},

            { "error_while_saving_data_message" ," Error while saving data to table."},
            { "error_while_saving_data_key" ," error.unableToSaveTransaction "},

            { "unable_to_process_payment_please_try_again_message" ," Unable to process payment message. Please try again,if the problem persists kindly contact our Merchant Hotline."},
            { "unable_to_process_payment_please_try_again_key" ," notification.service.down "},

            { "merchant_disallowed_magstripe_message" ," Merchant disallowed magstripe and signature verification.Please contact support."},
            { "merchant_disallowed_magstripe_key" ," error.disallow magstripe and signature"},
            { "system_is_currently_not_available_message" ," System is currently not available. Please try again later."},
            { "system_is_currently_not_available_key" ," error.systemIsdown "},

            { "transaction_must_use_pin_message" ," Transaction must use PIN."},
            { "transaction_must_use_pin_key" ," error.transactionMustUsePIN "},

            { "wrong_choice_of_transaction_type_please_use_credit_message" ," Wrong choice of transaction type: please use credit transaction."},
            { "wrong_choice_of_transaction_type_please_use_credit_key" ," error.transactionCreditForDebitCard "},

            { "wrong_choice_of_transaction_type_please_use_debit_message" ," Wrong choice of transaction type: please use debit transaction."},
            { "wrong_choice_of_transaction_type_please_use_debit_key" ," error.transactionDebitForCreditCard "},

            { "incorrect_pin_message" ," Incorrect PIN."},
            { "incorrect_pin_key" ," error.incorrectOfflinePIN "},

            { "duplicate_transaction_message" ," Duplicate Transaction."},
            { "duplicate_transaction_key" ," error.transactionDuplicateKSN "},

            { "your_transaction_is_below_than_limit_message" ," Your transaction is below than limit per transaction."},
            { "your_transaction_is_below_than_limit_key" ," error.belowTransactionLimit "},
            { "your_transaction_currency_is_not_supported_message" ," Your transaction currency is not supported."},
            { "your_transaction_currency_is_not_supported_key" ," error.currencyNotSupported "},

            { "transaction_amount_mismatch_message" ," Transaction amount mismatch between EMV amount and service amount."},
            { "transaction_amount_mismatch_key" ," error.amountMissMatched "},

            { "an_error_has_occurred_message" ," An error has occurred.Please contact our Merchant Hotline."},
            { "an_error_has_occurred_key" ," error.hostErrorGeneral "},

            { "host_busy_key" ," Connection Error.Please try again,if the problem persists kindly contact our Merchant Hotline."},
            { "batch_upload_message" ,"\"Batch Upload failed. Please call Help Desk."},
            { "batch_upload_key" ," error.hostErrorBatchUploadFailed "},

            { "host_response_key" ,"\"error.HostResponse\""},
            { "invalid_card_message" ," Invalid card."},
            { "invalid_card_key" ," error.payment - card - error "},

            { "invalid_service_name_message" ," Invalid service name/version"},
            { "invalid_service_name_key" ," error.service - not - found "},

            { "method_invocation_error" ," Method invocation error."},
            { "method_invocation_key" ," error.method - invocation "},

            { "no_application_id_selected_message" ," No Application ID is selected."},
            { "no_application_id_selected_key" ," error.payment - card - no - application - selected "},

            { "service_is_currently_unavailable_message" ," Service is currently unavailable.Please try again,if the problem persists kindly contact our Merchant Hotline."},
            { "service_is_currently_unavailable_key" ," error.unhandled - error "},

            { "reader_id_in_session_message" ," Reader ID in session and request dont match."},
            { "reader_id_in_session_key" ," error.reader - id - mismatch "},

            { "reader_id_does_not_exist_message" ," Reader ID does not exist in the concurrent map."},
            { "reader_id_does_not_exist_key" ," error.reader - id - does - not - exist "},

            { "connection_between_client_and_host_expired_message" ," Connection between client and host expired, due to cancellation or timeout."},
            { "connection_between_client_and_host_expired_key" ," error.transaction - transfer - object - expired "},

            { "maximum_thread_limit_reached_message" ," Maximum thread limit reached."},
            { "maximum_thread_limit_reached_key" ," error.thread - limit - reached "},

            { "thread_interrupted_in_long_poller_message" ," Thread interrupted in long poller, probably triggered by a forced destroy."},
            { "thread_interrupted_in_long_poller_key" ," error.thread - interrupted "},

            { "error_during_encryption_decryption_message" ," Error during encryption/decryption."},
            { "error_during_encryption_decryption_key" ," error.during - encryption - or - decryption "},

            { "error_client_disconnected_message" ," Error, client disconnected."},
            { "error_client_disconnected_key" ," error.client - disconnected "},

            { "login_token_could_not_be_created_message" ," Login token could not be created."},
            { "login_token_could_not_be_created_key" ," error.login - token - could - not - be - created "},

            { "login_token_could_not_be_found_message" ," Login token could not be found or found to be mismatched."},
            { "login_token_could_not_be_found_key" ," error.login - token - mismatch "},

            { "login_token_expired_message" ," Login token expired."},
            { "login_token_expired_key" ," error.login - token - expired "},

            { "problem_in_receiving_help_message" ," Problem in receiving help message."},
            { "problem_in_receiving_help_key" ," error.recieve - help - message "},

            { "requested_data_is_unavailable_message" ," Requested data is unavailable,if the problem persists kindly contact our Merchant Hotline."},
            { "requested_data_is_unavailable_key" ," error.no - such - data "},

            { "state_of_requested_data_is_invalid_message" ," State of requested data is invalid, please kindly contact our Merchant Hotline."},
            { "state_of_requested_data_is_invalid_key" ," error.invalid - data "},

            { "message_send_successfully" ," Message send successfully."},
            { "check_your_email" ," Check your email."},
            { "init_payment_success" ," Init payment success."},
            { "confirm_payment_success" ," Confirm payment success."},
            { "start_payment_success" ," Start payment success."},
            { "reversal_before_payment_success" ," Reversal before payment success."},
            { "decrypt_process_failed" ," Decrypt process failed."},
            { "connecting_to" ," Connecting to..."},
            { "no_printer_paired" ," You don\'t have Printer paired."},
            { "enable_bluetooth" ," Please enable bluetooth."},
            { "printer_connected_with" ," Printer connected with:"},
            { "check_your_printer" ," Check your printer."},
            { "please_try_again" ," Please try again."},
            { "please_provide_valid_payment_data" ," Please provide valid payment data."},
            { "battery_low" ," Printer battery low."},
            { "printing_finished" ," Printing finished."},
            { "paper_empty" ," Paper empty."},
            { "printer_overheat" ," Printer overheat."},
            { "reader_not_connected" ," Reader not connected."},
            { "reader_connected_with" ," Reader connected with:"},
            { "reader_disconnected" ," Reader Companion Disconnected."},
            { "please_provide_signature" ," Please provide valid signature."},
            { "remove_card" ," Please remove card."},
            { "check_your_reader" ," Check your reader companion."},
            { "amount_is_invalid" ," Amount is not valid."},
            { "provide_transaction_type" ," Please provide transaction type."},
            { "enable_gps" ," Please enable GPS."},
            { "provide_verification_mode" ," Please provide verification mode."},
            { "not_connect_with_reader_only_cash" ," You\'re not connecting with your Reader companion, only CASH Transaction can be proceed."},
            { "insert_card" ," Please insert card."},
            { "swipe_card" ," Please swipe card."},
            { "failed_get_serial_number" ," Failed get companion serial number, check your companion."},
            { "no_response_from_reader" ," No response from the companion reader."},
            { "reader_waiting_timeout" ," Reader waiting time out."},
            { "aborted_by_user" ," Transaction cancelled."},
            { "processing_error" ," Error while processing."},
            { "expired_card" ," Card expired."},
            { "invalid_card_data" ," Card data not valid."},
            { "transaction_declined" ," Transaction declined."},
            { "ipek_null" ," Reader not activated."},
            { "transaction_failed" ," Transaction failed."},
            { "username_is_mandatory" ," User name is mandatory."},
            { "password_is_mandatory" ," Password is mandatory."},

            { "payment_capability_success" ," Get payment capability success."},
            { "transaction_id_is_mandatory" ," Transaction id is mandatory."},
            { "cash_cannot_be_voided" ," Cash transaction can not be voided."},
            { "please_provide_message" ," Please provide your message."},
            { "username_and_pin_mandatory" ," Username and PIN is mandatory."},
            { "username_less_than_three" ," Username not less than 3 characters."},
            { "password_less_than_six" ," PIN not less than 6 characters."},
            { "provide_valid_username" ," Please provide valid user name."},
            { "provide_transaction_id" ," Please provide transaction id."},
            { "provide_valid_image_path" ," Please provide valid image path."},
            { "please_paired_your_reader" ," Please pair your Reader Companion."},
            { "wait_for_location" ," Please wait, updating location."},
            { "aggregator_login_data_mandatory" ," Server public key, Client private key, Mobile user id and Aggregator id are mandatory."},
            { "server_public_key_is_mandatory" ," Server public key is mandatory."},
            { "mobile_user_id_is_mandatory" ," Mobile user id is mandatory."},
            { "aggregator_id_is_mandatory" ," Aggregator id is mandatory."},


            { "please_insert_card" ," Please insert card"},
            { "please_swipe_card" ," Please swipe card"},
            { "please_swipe_insert_card" ," Please swipe/insert card"},
            { "please_tap_card" ," Please tap card"},
            { "please_swipe_tap_card" ," Please swipe/tap card"},
            { "please_insert_tap_card" ," Please insert/tap card"},
            { "please_swipe_insert_tap_card" ," Please swipe/insert/tap card"},
            { "no_card_detected" ," No card detected"},
            { "icc_card_inserted" ," ICC Card Inserted"},
            { "card_inserted" ," Card Inserted (Not ICC). Please swipe card"},
            { "bad_swipe" ," Bad Swipe"},
            { "card_swiped" ," Card Swiped: "},
            { "mag_head_fail" ," Magnetic head fail"},
            { "card_no_response" ," Check card no response"},
            { "card_swiped_track2_only" ," Card Swiped (Track 2 only):\n"},
            { "use_icc_card" ," Please use ICC card"},
            { "tap_card_detected" ," Tap card detected"},
            { "format_id" ," Format ID: "},
            { "masked_pan" ," Masked PAN: "},
            { "pan" ," PAN:"},
            { "expiry_date" ," Expiry Date: "},
            { "cardholder_name" ," Cardholder Name: "},
            { "ksn" ," KSN: "},
            { "epb" ," EPB: "},
            { "service_code" ," Service Code: "},
            { "track_1_length" ," Track 1 Length: "},
            { "track_2_length" ," Track 2 Length: "},
            { "track_3_length" ," Track 3 Length: "},
            { "encrypted_tracks" ," Encrypted Tracks: "},
            { "encrypted_track_1" ," Encrypted Track 1: "},
            { "encrypted_track_2" ," Encrypted Track 2: "},
            { "encrypted_track_3" ," Encrypted Track 3: "},
            { "track_1_status" ," Track 1 Status: "},
            { "track_2_status" ," Track 2 Status: "},
            { "track_3_status" ," Track 3 Status: "},
            { "partial_track" ," Partial Track: "},
            { "product_type" ," Product Type:"},
            { "final_message" ," Final Message:"},
            { "random_number" ," Random Number:"},
            { "encrypted_working_key" ," Encrypted Working Key:"},
            { "mac" ," MAC:"},
            { "card_data" ," Card Data: "},
            { "start_emv_fail" ," Start EMV failed"},
            { "start_emv_success" ," Start EMV success"},
            { "app_version" ," App Version: "},
            { "bootloader_version" ," Bootloader Version: "},
            { "firmware_version" ," Firmware Version: "},
            { "main_processor_version" ," Main Processor Version: "},
            { "coprocessor_version" ," Coprocessor Version: "},
            { "coprocessor_bootloader_version" ," Coprocessor Bootloader Version: "},
            { "usb" ," USB: "},
            { "charge" ," Charge: "},
            { "battery_level" ," Battery Level: "},
            { "battery_percentage" ," Battery Percentage:"},
            { "hardware_version" ," Hardware Version: "},
            { "supported_track" ," Supported Track: "},
            { "track_1_supported" ," Track 1 Supported: "},
            { "track_2_supported" ," Track 2 Supported: "},
            { "track_3_supported" ," Track 3 Supported: "},
            { "product_id" ," Product ID:"},
            { "pin_ksn" ," PIN KSN: "},
            { "emv_ksn" ," EMV KSN: "},
            { "track_ksn" ," Track KSN: "},
            { "mac_ksn" ," MAC KSN: "},
            { "nfc_ksn" ," NFC KSN: "},
            { "message_ksn" ," Message KSN: "},
            { "csn" ," CSN: "},
            { "uid" ," UID: "},
            { "vendor_id" ," Vendor ID:"},
            { "vendor_id_hex" ," Vendor ID (HEX):"},
            { "vendor_id_ascii" ," Vendor ID(ASCII):"},
            { "terminal_setting_version" ," Terminal Setting Version:"},
            { "device_setting_version" ," Device Setting Version:"},
            { "serial_number" ," Serial Number:"},
            { "model_name" ," Model Name: "},
            { "unknown" ," Unknown "},
            {"set_amount","Set Amount"},
            { "enter_pin" ," Please enter PIN"},
            { "enter_pin_on_keypad" ," Please enter PIN on keypad"},
            { "online_process_requested" ," Online process requested."},
            { "request_data_to_server" ," Request data to server."},
            { "replied_connected" ," Replied connected."},
            { "replied_success" ," Replied success."},
            { "replied_failed" ," Replied failed."},
            { "batch_data" ," Batch Data:\n"},
            { "transaction_log" ," Transaction Log:\n"},
            { "load_log" ," Load Log:\n"},
            { "reversal_data" ," Reversal Data:\n"},
            { "please_select_app" ," Please select app"},
            { "transaction_result" ," Transaction Result"},
            { "transaction_approved" ," Approved "},

            { "transaction_terminated" ," Terminated "},

            { "transaction_cancel" ," Cancel "},

            { "transaction_capk_fail" ," Fail(CAPK fail) "},
            { "transaction_not_icc" ," Fail(Not ICC card) "},

            { "transaction_app_fail" ," Fail(App fail) "},

            { "transaction_device_error" ," Device Error"},
            { "transaction_application_blocked" ," Application Blocked"},
            { "transaction_icc_card_removed" ," ICC card removed"},
            { "transaction_card_blocked" ," Card blocked"},
            { "transaction_card_not_supported" ," Card not support"},
            { "transaction_condition_not_satisfied" ," Condition of use not satisfied"},
            { "transaction_invalid_icc_data" ," Invalid ICC data"},
            { "transaction_missing_mandatory_data" ," Missing mandatory data"},
            { "transaction_no_emv_apps" ," No EMV apps"},
            { "tlv_list" ," TLV List:\n"},
            { "advice_process" ," Advice Process."},
            { "call_your_bank" ," Please call your bank"},
            { "request_terminal_time" ," Request Terminal Time. Replied"},
            { "confirm_amount" ," Confirm amount"},
            { "amount" ," Amount "},

            { "amount_ok" ," Amount OK?"},
            { "approved" ," Approved "},

            { "cancel_or_enter" ," Cancel or Enter"},
            { "card_error" ," Card error"},
            { "declined" ," Declined "},

            { "enter_amount" ," Please enter amount"},
            { "incorrect_pin" ," Incorrect PIN"},
            { "not_accepted" ," Not accepted"},
            { "pin_ok" ," PIN OK"},
            { "wait" ," Please wait&#8230;"},
            { "use_chip_reader" ," Please use chip reader"},
            { "use_mag_stripe" ," Please use mag stripe"},
            { "try_again" ," Please try again"},
            { "refer_payment_device" ," Please refer to your payment device"},
            { "try_another_interface" ," Please try another interface"},
            { "online_required" ," Online required"},
            { "processing" ," Processing &#8230;"},
            {"welcome","Welcome"},
            { "present_one_card" ," Please present only one card"},
            { "capk_failed" ," CAPK loading failed"},
            { "last_pin_try" ," Last PIN try"},
            { "select_account" ," Please select account"},
            { "insert_or_tap_card" ," Please insert or tap card"},
            { "approved_please_sign" ," Approved.Please sign"},
            { "tap_card_again" ," Please tap card again"},
            { "authorising" ," Authorising &#8230;"},
            { "insert_or_swipe_card_or_tap_another_card" ," Please insert, swipe or try another card"},
            { "insert_swipe_or_try_another_card" ," Please insert, swipe or try another card"},
            { "insert_or_swipe_card" ," Please insert or swipe card"},
            { "multiple_cards_detected" ," Multiple card detected"},
            { "application_expired" ," Application expired"},
            { "show_thank_you" ," Show thank you"},
            { "pin_try_limit_exceeded" ," PIN try limit exceeded"},
            { "final_confirm" ," Final Confirm"},
            { "battery_critically_low" ," Device battery critically low and powered off"},
            { "no_device_detected" ," No device detected."},
            { "device_plugged" ," Device plugged."},
            { "device_unplugged" ," Device unplugged."},
            { "command_not_available" ," Command not available"},
            { "device_no_response" ," Device no response"},
            { "device_reset" ," Device reset"},
            { "unknown_error" ," Unknown error"},
            { "device_busy" ," Device Busy"},
            { "out_of_range" ," Input out of range."},
            { "invalid_format" ," Input invalid format."},
            { "zero_values" ," Input are zero values."},
            { "input_invalid" ," Input invalid."},
            { "cashback_not_supported" ," Cashback not supported."},
            { "crc_error" ," CRC Error."},
            { "comm_error" ," Communication Error."},
            { "device_off" ," Device powered off."},
            { "get_info" ," Get Device Info"},
            { "clear_log" ," Clear Log"},
            { "check_card" ," Check Card"},
            { "start_emv" ," Start EMV"},
            { "cancel" ," Cancel "},

            { "pin" ," PIN "},

            { "cashback_amount" ," Cashback "},

            { "confirm" ," Confirm "},

            { "bypass" ," Bypass "},

            { "decline" ," Decline "},

            { "connection" ," Connection "},

            { "start_connection" ," Start Connection"},
            { "stop_connection" ," Stop Connection"},
            { "bluetooth_devices" ," Bluetooth Devices"},
            { "paired_devices" ," Paired Devices"},
            { "discovered_devices" ," Discovered Devices"},
            { "scan_and_connect" ," Scan and Connect"},
            { "scanning_bluetooth_2" ," Scanning Bluetooth 2&#8230;"},
            { "scanning_bluetooth_4" ," Scanning Bluetooth 4&#8230;"},
            { "btv4_devices" ," Bluetooth 4 Devices"},
            { "start_bluetooth_server" ," Start Bluetooth Server"},
            { "stop_bluetooth" ," Stop Bluetooth"},
            { "unpair_all" ," Unpair All"},
            { "bluetooth_2_detected" ," Bluetooth 2 detected"},
            { "connecting_bluetooth" ," Connecting Bluetooth&#8230;"},
            { "connecting_bluetooth_2" ," Connecting Bluetooth 2&#8230;"},
            { "connecting_bluetooth_4" ," Connecting Bluetooth 4&#8230;"},
            { "bluetooth_connected" ," Bluetooth connected"},
            { "bluetooth_disconnected" ," Bluetooth disconnected"},
            { "bluetooth_2_connected" ," Bluetooth 2 connected"},
            { "bluetooth_2_disconnected" ," Bluetooth 2 disconnected"},
            { "bluetooth_4_connected" ," Bluetooth 4 connected"},
            { "bluetooth_4_disconnected" ," Bluetooth 4 disconnected"},
            { "bluetooth_scan_timeout" ," Bluetooth scan timeout"},
            { "bluetooth_scan_stopped" ," Bluetooth scan stopped"},
            { "bluetooth_2_scan_timeout" ," Bluetooth 2 scan timeout"},
            { "bluetooth_2_scan_stopped" ," Bluetooth 2 scan stopped"},
            { "bluetooth_4_scan_timeout" ," Bluetooth 4 scan timeout"},
            { "bluetooth_4_scan_stopped" ," Bluetooth 4 scan stopped"},
            { "serial_connected" ," Serial connected"},
            { "please_wait" ," Please wait&#8230;"},
            { "initializing" ," Initializing &#8230;"},
            { "serial_disconnected" ," Serial disconnected"},
            { "audio_started" ," Audio started"},
            { "amount_confirmed" ," Amount confirmed"},
            { "amount_canceled" ," Amount canceled"},
            { "please_swipe_or_insert" ," Please insert or swipe card"},
            { "pin_entered" ," PIN entered"},
            { "pin_bypassed" ," PIN bypassed"},
            { "pin_canceled" ," PIN canceled"},
            { "pin_timeout" ," PIN timeout"},
            { "key_error" ," Key error"},
            { "no_pin" ," No PIN"},
            { "fail_to_start_audio" ," Fail to start audio"},
            { "fail_to_start_bluetooth" ," Fail to start bluetooth"},
            { "fail_to_start_bluetooth_v2" ," Fail to start bluetooth 2.0"},
            { "fail_to_start_bluetooth_v4" ," Fail to start bluetooth 4.0"},
            { "invalid_function" ," Invalid function"},
            { "comm_link_uninitialized" ," Comm link uninitialized"},
            { "bluetooth_2_already_started" ," Error: Bluetooth 2 already started"},
            { "bluetooth_4_already_started" ," Error: Bluetooth 4 already started"},
            { "bluetooth_4_not_supported" ," Bluetooth 4 not supported"},
            { "channel_buffer_full" ," Channel buffer full"},
            { "bluetooth_permission_denied" ," Bluetooth permission denied"},
            { "volume_warning_not_accepted" ," Volume warning not accepted"},
            { "fail_to_start_serial" ," Fail to start serial"},
            { "usb_device_not_found" ," USB device not found"},
            { "usb_device_permission_denied" ," USB device permission denied"},
            { "usb_not_supported" ," USB not supported"},
            { "error_message" ," Error message:"},
            { "getting_info" ," Getting device info&#8230;"},
            { "starting" ," Starting &#8230;"},
            { "please_confirm_amount" ," Please confirm amount"},
            { "start" ," Start "},

            { "cancel_check_card_success" ," Cancel check card success"},
            { "cancel_check_card_fail" ," Cancel check card fail"},
            { "printer_command_success" ," Printer command success"},
            { "no_paper" ," No paper"},
            { "wrong_printer_cmd" ," Wrong printer command"},
            { "printer_error" ," Printer error"},
            { "request_printer_data" ," Request printer data:"},
            { "request_reprint_data" ," Request reprint data:"},
            { "printer_operation_cancelled" ," Printer operation cancelled"},
            { "printer_operation_end" ," Printer operation end"},
            { "amount_with_colon" ," Amount:"},
            { "cashback_with_colon" ," Cashback:"},
            { "currency_with_colon" ," Currency:"},
            { "enable_input_amount" ," Enable Input Amount"},
            { "enable_input_amount_success" ," Enable input amount success"},
            { "enable_input_amount_fail" ," Enable input amount fail"},
            { "disable_input_amount_success" ," Disable input amount success"},
            { "disable_input_amount_fail" ," Disable input amount fail"},
            { "phone_number" ," Phone No.:"},
            { "timeout" ," Timeout "},

            { "canceled" ," Canceled "},

            { "wrong_length" ," Wrong Length"},
            { "emv_card_data_result" ," EMV card data result:"},
            { "emv_card_data_failed" ," Get EMV card data failed"},
            { "emv_card_balance_result" ," EMV card data result:"},
            { "emv_card_balance_failed" ," Get EMV card data failed"},
            { "encrypted_data" ," Encrypted Data:"},
            { "please_use_chip_card" ," Please use chip card"},
            { "success" ," Success "},

            { "fail" ," Fail "},

            { "verify_id" ," Please Verfiy Cardholder ID"},
            { "cardholder_certificate" ," Cardholder Certificate:"},
            { "certificate_type" ," Certificate Type:"},
            { "receipt_data" ," Receipt Data:"},
            { "update_terminal_setting_success" ," Update terminal setting success"},
            { "update_terminal_setting_tag_not_found" ," Update terminal setting failed.Tag not found"},
            { "update_terminal_setting_length_incorrect" ," Update terminal setting failed.Length incorrect"},
            { "update_terminal_setting_tlv_incorrect" ," Update terminal setting failed.TLV incorrect"},
            { "update_terminal_setting_bootloader_not_support" ," Update terminal setting failed.Bootloader not support"},
            {  "update_terminal_setting_tag_not_allowed_to_change" ," Update terminal setting failed.Tag not allowed to change"},
            { "update_terminal_setting_tag_not_allowed_to_access" ," Update terminal setting failed.Tag not allowed to access"},
            { "update_terminal_setting_user_defined_data_not_allowed_to_change" ," Update terminal setting failed.User defined data not allowed to change"},
            { "update_terminal_setting_tag_not_written_correctly" ," Update terminal setting failed.Tag not written correctly"},
            { "read_terminal_setting_success" ," Read terminal setting success"},
            { "read_terminal_setting_tag_not_found" ," Read terminal setting failed.Tag not found"},
            { "read_terminal_setting_length_incorrect" ," Read terminal setting failed.Length incorrect"},
            { "read_terminal_setting_tlv_incorrect" ," Read terminal setting failed.TLV incorrect"},
            { "read_terminal_setting_bootloader_not_support" ," Read terminal setting failed.Bootloader not support"},
            { "read_terminal_setting_tag_not_allowed_to_change" ," Read terminal setting failed.Tag not allowed to change"},
            { "read_terminal_setting_tag_not_allowed_to_access" ," Read terminal setting failed.Tag not allowed to access"},
            { "read_terminal_setting_user_defined_data_not_allowed_to_change" ," Read terminal setting failed.User defined data not allowed to change"},
            { "read_terminal_setting_tag_not_written_correctly" ," Read terminal setting failed.Tag not written correctly"},
            { "value" ," Value:"},
            { "print_sample" ," Print Sample (M361)"},
            { "device_not_support_printing" ," Device not support printing"},
            { "inject_master_key" ," Inject Master Key"},
            { "inject_session_key" ," Inject Session Key"},
            { "disable_inject_master_key" ," Disable Inject Master Key"},
            { "inject_master_key_success" ," Inject Master Key success"},
            { "inject_master_key_fail" ," Inject master key fail"},
            { "disable_inject_master_key_success" ," Disable inject master key success"},
            { "disable_inject_master_key_fail" ," Disable inject master key fail"},
            { "key_exchange_success" ," Key exchange success"},
            { "key_exchange_fail" ," Key exchange fail"},
            { "key_exchange" ," Key Exchange"},
            { "message" ," Message:"},
            { "data" ," Data:"},
            { "sending_encrypted_pin_session_key" ," Sending encrypted PIN Session Key&#8230;"},
            { "sending_encrypted_data_session_key" ," Sending encrypted Data Session Key&#8230;"},
            { "sending_encrypted_track_session_key" ," Sending encrypted Track Session Key&#8230;"},
            { "sending_encrypted_mac_session_key" ," Sending encrypted MAC Session Key&#8230;"},
            { "inject_session_key_success" ," Inject Session Key success"},
            { "inject_session_key_failed" ," Inject Session Key failed"},
            { "apdu_result" ," APDU Result:"},
            { "encrypt_data" ," Encrypt Data"},
            { "encrypt_pin" ," Encrypt Pin"},
            { "encrypt_data_failed" ," Encrypt data failed"},
            { "please_press_reprint_or_print_next" ," Please press reprint or print next"},
            { "apdu" ," APDU "},

            { "title_activity_apdu" ," WisePadAPI - Example - 2.5.1"},
            { "power_on_icc" ," Power on ICC"},
            { "power_off_icc" ," Power off ICC"},
            { "send_apdu" ," Send APDU"},
            { "no_aid_matched" ," No AID matched"},
            { "sending" ," Sending:"},
            { "please_power_on_icc" ," Please power on ICC first"},
            { "apdu_failed" ," APDU Failed"},
            { "power_on_icc_success" ," Power on ICC Success"},
            { "atr" ," ATR:"},
            { "atr_length" ," ATR Length:"},
            { "power_on_icc_failed" ," Power on ICC Failed"},
            { "power_off_icc_success" ," Power off ICC Success"},
            { "power_off_icc_failed" ," Power off ICC Failed"},
            { "main" ," MainActivity "},

            { "swipe" ," Swipe "},

            { "insert" ," Insert "},

            { "tap" ," Tap "},

            { "swipe_or_insert" ," Swipe or Insert"},
            { "swipe_or_tap" ," Swipe / Tap "},

            { "insert_or_tap" ," Insert / Tap "},

            { "swipe_or_insert_or_tap" ," Swipe / Insert / Tap "},

            { "select_mode" ," Select Mode"},
            { "track_encoding" ," Track Encoding:"},
            { "injectmk" ," Inject Master Key"},
            { "bdk" ," BDK "},

            { "mk" ," Master Key"},
            { "ks" ," KSN "},

            { "get_capk_list" ," Get CAPK List"},
            { "get_capk_detail" ," Get CAPK Detail"},
            { "find_capk" ," Find CAPK"},
            { "update_capk" ," Update CAPK"},
            { "get_emv_report_list" ," Get EMV Report List"},
            { "get_emv_report" ," Get EMV Report"},
            { "capk" ," CAPK:"},
            { "location" ," Location:"},
            { "rid" ," RID:"},
            { "index" ," Index:"},
            { "exponent" ," Exponent:"},
            { "modulus" ," Modulus:"},
            { "checksum" ," Checksum:"},
            { "size" ," Size:"},
            { "update_capk_success" ," Update CAPK success"},
            { "update_capk_fail" ," Update CAPK fail"},
            { "emv_report_list" ," EMV report list:"},
            { "emv_report" ," EMV report: "},
            { "menu_capk" ," CAPK "},

            { "session_error_firmware_not_supported" ," Firmware not supported"},
            { "session_error_invalid_session" ," Invalid session"},
            { "session_error_invalid_vendor_token" ," Invalid vendor token"},
            { "session_error_session_not_initialized" ," Session not initialized"},
            { "init_session" ," Initialize session"},
            { "reset_session" ," Reset session"},
            { "is_session_initialized" ," Is session initialized"},
            { "session_initialized" ," Session initialized"},
            { "session_not_initialized" ," Session not initialized"},
            { "initializing_session" ," Initializing session..."},
            { "update_gprs_setting" ," Update GPRS"},
            { "update_wifi_setting" ," Update WIFI"},
            { "read_gprs_setting" ," Read GPRS"},
            { "read_wifi_setting" ," Read WIFI"},
            { "update_gprs_setting_success" ," Update GPRS setting success"},
            { "update_gprs_setting_fail" ," Update GPRS setting fail"},
            { "update_wifi_setting_success" ," Update WIFI setting success"},
            { "update_wifi_setting_fail" ," Update WIFI setting fail"},
            { "read_gprs_setting_success" ," Read GPRS setting success"},
            { "read_gprs_setting_fail" ," Read GPRS setting fail"},
            { "read_wifi_setting_success" ," Read WIFI setting success"},
            { "read_wifi_setting_fail" ," Read WIFI setting fail"},
            { "operator" ," Operator : "},
            { "apn" ," APN : "},
            { "username" ," Username : "},
            { "password" ," Password : "},
            { "ssid" ," SSID : "},
            { "url" ," URL : "},
            { "portNumber" ," Port Number : "},
            { "gprs" ," GPRS "},

            { "wifi" ," WIFI "},

            { "nfc" ," NFC "},

            { "terminal_setting_status" ," Terminal setting connected : "},
            { "length_incorrect" ," Length incorrect"},
            { "tlv_incorrect" ," TLV incorrect"},
            { "tag_not_found" ," Tag not found"},
            { "tag_incorrect" ," Tag incorrect"},
            { "bootloader_not_support" ," Bootloader not support"},
            { "tag_not_allowed_to_change" ," Tag not allowed to change"},
            { "tag_not_allowed_to_access" ," Tag not allowed to access"},
            { "user_defined_data_not_allowed_to_change" ," User defined data not allowed to change"},
            { "tag_not_written_correctly" ," Tag not written correctly"},
            { "unpair_all_start" ," Unpair all begin. Please wait"},
            { "unpair_all_end" ," Unpair all finish"},
            { "unpair_all_fail" ," Unpair all fail"},
            { "gprs_wifi" ," GPRS / WIFI "},

            { "track_2_equivalent_data" ," Track 2 Equivalent Data: "},
            { "ksn_of_batch_data" ," KSN of batch data: "},
            { "ksn_of_tag_57" ," KSN of Tag 57: "},
            { "date" ," Date: "},
            { "time" ," Time: "},
            { "b_id" ," bID "},

            { "usb_connected" ," USB connected"},
            { "usb_disconnected" ," USB disconnected"},
            { "cancel_check_card" ," Cancel Check Card"},
            { "auto_config" ," Auto Config"},
            { "auto_configuring" ," Auto Configuring&#8230;"},
            { "auto_config_completed" ," Auto config completed"},
            { "settings" ," Settings:"},
            { "default_settings" ," Default settings"},
            { "settings_written_to_external_storage" ," Settings written to external storage"},
            { "auto_config_error_phone_not_supported" ," Auto config error: phone not supported"},
            { "auto_config_error_interrupted" ," Auto config error: interrupted"},
            { "canceling_auto_config" ," Canceling auto config&#8230;"},
            { "setting_config" ," Setting config&#8230;"},
            { "setting_config_from_web_service" ," Setting config from web service"},
            { "start_nfc_detection" ," Start NFC detection"},
            { "stop_nfc_detection" ," Stop NFC detection"},
            { "nfc_data_exchange_write" ," NFC data exchange (Write)"},
            { "nfc_data_exchange_read_1st" ," NFC data exchange(Read 1st)"},
            { "nfc_data_exchange_read_next" ," NFC data exchange(Read next)"},
            { "nfc_data_exchange_success" ," NFC data exchange success"},
            { "nfc_data_exchange_fail" ," NFC data exchange fail"},
            { "ndef_record" ," NDEF Record : "},
            { "nfc_card_detection_result" ," NFC card detection result : "},
            { "nfc_tag_information" ," NFC tag information : "},
            { "nfc_card_uid" ," NFC card UID : "},
            { "public_key_version" ," Public Key Version"},
            { "transaction_cancelled" ," Transaction cancelled."},
            { "printing_cancelled" ," Printing cancelled."},

            { "reader_not_enable" ," You\'re not connecting with your Reader companion, only CASH Transaction can be proceed."},
            { "waiting_for_serial_number" ," Menunggu koneksi reader"},
            { "invalid_tid" ," No TID supported for current transaction."},
            { "invalid_tid_key" ," error.tidInvalid."},

            { "mobile_user_already_exist" ," MobileUser already exist with that name."},
            { "mobile_user_already_exist_key" ," error.UserDuplicate "},

            { "batch_is_full" ," Batch is full, please settle."},
            { "batch_is_full_key" ," error.settlement.required "},

            { "transaction_out_of_range" ," You cannot perform transaction outside permitted location."},
            { "transaction_out_of_range_key" ," error.locationOutOfRange "},

            { "invalid_aggregator" ," no aggregator supported for current transaction."},
            { "invalid_aggregator_key" ," error.aggregator "},

            { "invalid_request_url" ," invalid request url."},
            { "invalid_request_url_key" ," error.http.url "},

            { "card_not_support" ," Card not supported for current transaction."},
            { "card_not_support_key" ," error.invalidCardInstallment "},

            { "no_such_data" ," Requested data is unavailable,if the problem persists kindly contact our Merchant Hotline."},
            { "no_such_data_key" ," error.no - such - data "},

            { "data_error" ," State of requested data is invalid, please kindly contact our Merchant Hotline."},
            { "data_error_key" ," error.invalid - data "},


            { "username_and_pin_required" ," Username and PIN required."},
            { "username_required" ," User name required."},
            { "pin_required" ," Pin required."},
            { "username_too_short" ," Username too short."},
            { "pin_too_short" ," Pin too short."},
            { "username_pin_too_short" ," Username and Pin too short."},
            { "uploade_image_failed" ," Upload image failed."},
            { "image_already_exist" ," Image already exist."},
            { "transaction_id_required" ," Transaction Id required."},
            { "download_image_failed" ," Downlaod image failed."},
            { "google_play_service_not_available" ," Google Play Service is not available."},
            { "google_play_service_need_update" ," Please update your Google play service to continue the process."},
            { "no_reader_paired" ," No reader compainon paired."},
            { "bluetooth_off" ," Bluetooth off."},
            { "printer_failed" ," Connect to printer failed."},
            { "printer_off" ," Printer off."},
            { "failed_to_start_reader" ," Failed to start reader connection."},
            { "user_data_required" ," User data is mandatory."},
            { "invalid_exchange_key" ," Invalid exchange key."},
            { "encryption_error" ," Encryption process error."},
        };
        readonly private Dictionary<int, String> ErrorCodeToMsgCode= new Dictionary<int, string>{
           {1001, "username_and_pin_required"},{
           1002, "username_required"},{
           1003, "pin_required"},{
           1004, "username_too_short"},{
           1004, "pin_too_short"},{
           1005, "username_pin_too_short"},{
           1006, "aggregator_login_data_mandatory"},{
           1007, "server_public_key_is_mandatory"},{
           1008, "aggregator_id_is_mandatory"},{
           1009, "activation_validation"},{
           1010, "handshake_failed_info"},{
           1011, "decrypt_process_failed"},{
           1012, "check_reader_validation"},{
           1013, "please_provide_message"},{
           1014, "provide_valid_image_path"},{
           1015, "uploade_image_failed"},{
           1016, "image_already_exist"},{
           1017, "transaction_id_required"},{
           1018, "download_image_failed"},{
           1019, "please_provide_valid_payment_data"},{
           1020, "google_play_service_not_available"},{
           1021, "google_play_service_need_update"},{
           1022, "please_provide_signature"},{
           1023, "amount_is_invalid"},{
           1024, "enable_gps"},{
           1025, "wait_for_location"},{
           1026, "provide_transaction_type"},{
           1027, "no_reader_paired"},{
           1028, "no_printer_paired"},{
           1029, "bluetooth_off"},{
           1030, "printer_failed"},{
           1031, "printer_off"},{
           1032, "printer_overheat"},{
           1033, "paper_empty"},{
           1034, "please_try_again"},{
           1035, "battery_low"},{
           1036, "provide_verification_mode"},{
           1037, "not_connect_with_reader_only_cash"},{
           1038, "waiting_for_serial_number"},{
           1039, "failed_get_serial_number"},{
           1040, "reader_not_connected"},{
           1041, "failed_to_start_reader"},{
           1042, "reader_waiting_timeout"},{
           1043, "aborted_by_user"},{
           1044,  "processing_error"},{
           1045, "expired_card"},{
           1046, "invalid_card_data"},{
           1047, "transaction_declined"},{
           1048, "ipek_null"},{
           1049, "transaction_failed"},{
           1050, "password_is_mandatory"},{
           1051, "user_data_required"},{
           1052, "invalid_exchange_key"},{
           1053, "encryption_error"},{

           2001, "initialization_error_message"},{
           2002, "session_expired_message"},{
           2003, "tle_ltwk_key_download_error_message"},{
           2004, "tle_logon_download_error_message"},{
           2012, "page_number_is_invalid_message"},{

           3010, "exceed_three_attemps_message"},{
           3011, "exceed_five_attemps_message"},{
           3012, "not_authorized_user_message"},{
           3020, "device_not_unique_message"},{
           3021, "device_not_belong_to_bank_message"},{
           3022, "please_use_same_smart_reader_message"},{
           3023, "invalid_phone_id_message"},{
           3030, "reader_not_link_message"},{
           3031, "reader_is_inactive_or_suspended_message"},{
           3032, "reader_mulfunction_message"},{
           3040, "tid_is_suspended_or_not_linked_message"},{
           3042, "no_tid_is_linked_message"},{
           3043, "application_expired_message"},{
           3044, "new_application_available_message"},{

            // recoverable error - start
           5010, "invalid_login_message"},{
           5011, "user_pin_has_to_be_a_6_message"},{
           5012, "please_do_not_reuse_the_last_5_passwor_message"},{
           5013, "invalid_activation_code_message"},{
           5014, "please_ensure_user_id_and_pin_are_valid_message"},{
           5015, "user_is_not_active_message"},{
           5016, "activation_failed_message"},{
           5017, "mobile_user_already_exist"},{

           5020, "you_are_using_an_outdated_application_message"},{
           5030, "unable_to_find_resource_message"},{
           5031, "password_must_has_6_numbers_message"},{
           5032, "old_password_must_be_different_with_new_password_message"},{
           5033, "new_password_already_used_message"},{
           5034, "wrong_password_when_voiding_message"},{
           5035, "you_are_not_authorised_to_void_message"},{
           5036, "void_failed_because_this_user_is_suspended_message"},{
           5037, "settlement_failed_because_this_user_is_suspended_message"},{
           5038, "invalid_format_user_login_message"},{
           5039, "wrong_password_when_settlement_message"},{
           5040, "you_are_not_authorised_to_settle_message"},{
           5041, "failed_to_do_settlement_message"},{
           5042, "batch_is_full"},{
           5043, "unable_to_find_transaction_message"},{

           5110, "connection_error_message"},{
           5111, "you_have_exceeded_your_daily_limit_message"},{
           5112, "you_have_exceeded_your_monthly_limit_message"},{
           5113, "you_have_exceeded_your_transaction_limit_message"},{
           5114, "please_verify_mobile_number_message"},{
           5115, "please_verify_email_message"},{
           5116, "email_or_sms_service_is_currently_unavailable_message"},{
           5117, "your_transaction_is_not_allowed_by_risk_management_message"},{
           5118, "unable_to_process_payment_message"},{
           5119, "invalid_template_sms_message"},{
           5120, "error_while_saving_data_message"},{

           5121, "unable_to_process_payment_please_try_again_message"},{
           5122, "transaction_out_of_range"},{
           5123, "your_transaction_is_below_than_limit_message"},{
           5124, "your_transaction_currency_is_not_supported_message"},{
           5125, "transaction_amount_mismatch_message"},{
           5126, "transaction_already_reversed_message"},{
           5127, "invalid_tid"},{
           5128, "merchant_disallowed_magstripe_message"},{
           5129, "invalid_aggregator"},{
           5130, "invalid_request_url"},{
           5131, "card_not_support"},{

           5555, "system_is_currently_not_available_message"},{

           5600, "transaction_must_use_pin_message"},{
           5601, "wrong_choice_of_transaction_type_please_use_credit_message"},{
           5602, "wrong_choice_of_transaction_type_please_use_debit_message"},{
           5603, "incorrect_pin_message"},{
           5603, "duplicate_transaction_message"},{


           8090, "an_error_has_occurred_message"},{
           8091, "connection_error_message"},{
           8092, "connection_error_message"},{
           8093, "batch_upload_message"},{
           8094, "connection_error_message"},{

           9001, "invalid_card_message"},{
           9010, "invalid_service_name_message"},{
           9011, "method_invocation_error"},{
           9012, "no_application_id_selected_message"},{

           10001, "service_is_currently_unavailable_message"},{

           11001, "reader_id_in_session_message"},{
           11002, "reader_id_does_not_exist_message"},{

           12001, "connection_between_client_and_host_expired_message"},{
           12002, "maximum_thread_limit_reached_message"},{
           12003, "thread_interrupted_in_long_poller_message"},{

           13001, "error_during_encryption_decryption_message"},{
           13002, "error_client_disconnected_message"},{

           14001, "time_out_message"},{
           14002, "login_token_could_not_be_created_message"},{
           14003, "login_token_could_not_be_found_message"},{
           14004, "login_token_expired_message"},{
           15001, "problem_in_receiving_help_message)"}
        };
        protected int errorCode;
        protected String msgCode;

        public int getErrorCode() {
            return this.errorCode;
        }
        public void setErrorCode(int kodesalah) {
            this.errorCode = kodesalah;
        }

        public String getMsgCode() {
            return this.msgCode;
        }

        public void setMsgCode(String kodepesan) {
            this.msgCode = kodepesan;
        }

        public String getMsgfromErrorCode(int kodesalah) {
            return this.ErrorMessage[this.ErrorCodeToMsgCode[kodesalah]];
        }

        public String getMsgfromMsgCode(String kodepesan) {
            return this.ErrorMessage[kodepesan];
        }
    }
}

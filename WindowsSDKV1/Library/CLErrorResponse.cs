﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLErrorResponse
    {
        private int errorCode;
        private String errorMessage;
        private int errorHostCode;
        private String errorHostMessage;
        private int httpStatusCode;

        public CLErrorResponse() {

        }

        public CLErrorResponse(int kodesalah, String messagesalah) {
            this.errorCode = kodesalah;
            this.errorMessage = messagesalah;
        }

        public int getErrorCode() { return this.errorCode; }

        public void setErrorCode(int kodesalah) { this.errorCode = kodesalah; }

        public String getErrorMessage() { return this.errorMessage; }

        public void setErrorMessage(String pesansalah) { this.errorMessage = pesansalah; }

        public int getErrorHostCode() { return this.errorHostCode; }

        public void setErrorHostCode(int kodesalahhost) { this.errorHostCode = kodesalahhost; }

        public String getErrorHostMessage() { return this.errorHostMessage; }

        public void setErrorHostMessage(String pesansalahhost) { this.errorHostMessage = pesansalahhost; }

        public int getHttpStatusCode() { return this.httpStatusCode; }

        public void setHttpStatusCode(int httpcode) { this.httpStatusCode = httpcode; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLLoginResponse : CLResponse
    {
        private String userName;
        private CLMerchant merchant;
        //private CLPaymentCapability 

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(string un) {
            this.userName = un;
        }

        public CLMerchant getMerchant() {
            return this.merchant;
        }

        public void setMerchant(CLMerchant merch) {
            this.merchant = merch;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLCardSchemeElement
    {
        protected String appIdentifier;
        protected String descriptionalName;
        protected String additionalInfo;

        public CLCardSchemeElement(String appidentity, String descname, String addinfo) {
            this.appIdentifier = appidentity;
            this.descriptionalName = descname;
            this.additionalInfo = addinfo;
        }

        public String getAppIdentifier() {
            return this.appIdentifier;
        }

        public void setAppIdentifier(String appidentity) {
            this.appIdentifier = appidentity;
        }

        public String getDescriptionalName() {
            return this.descriptionalName;
        }

        public void setDescriptionalName(String descName) {
            this.descriptionalName = descName;
        }

        public String getAdditionalinfo() {
            return this.additionalInfo;
        }

        public void setAdditionalInfo(String addinfo) {
            this.additionalInfo = addinfo;
        }
    }
}

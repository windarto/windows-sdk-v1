﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class PlatformTypeEnum
    {
        public Dictionary<string, string> PlatformType = new Dictionary<string, string> {
            {"ANDROID","ANDROID"},
            {"IOS","IOS"},
            {"WIN_DESKTOP","WIN_DESKTOP"},
            {"WIN_PHONE","WIN_PHONE"},
            {"WIN_UNIVERSAL","WIN_UNIVERSAL"}
        };

        private String value;

        public PlatformTypeEnum(String Value)
        {
            this.value = this.PlatformType[Value];
        }

        public String getValue() {
            return this.value;
        }

    }
}

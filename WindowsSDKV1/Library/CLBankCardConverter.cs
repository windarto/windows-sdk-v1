﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLBankCardConverter
    {
        public static string getBankName(string bankcode)
        {
            string bankName;
            switch (bankcode) {
                case "0022":
                    bankName = "BRI";
                    break;
                case "0008":
                    bankName = "Mandiri";
                    break;
                case "0009":
                    bankName = "BNI";
                    break;
                case "0016":
                    bankName = "Maybank";
                    break;
                default:
                    throw new ArgumentException("Invalid Bank Code" + bankcode);
            }
            return bankName;
        }

        public static string getBankCode(string fullCode) {
            return fullCode.Substring(0, 4);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    class CLBasePayment
    {
        protected String id;
        protected String amount;
        protected TransactionType transactionType;
        protected String merchantTransactionID;
        protected CLTransactionNameEnum transactionNameEnum;

        public CLBasePayment(TransactionTipe transactionType) {
            this.transactionType = new TransactionType((int)transactionType);
        }

        public TransactionType getTransactionType() {
            return this.transactionType;
        }

        public void setTransactionType(TransactionType transtype) {
            this.transactionType = transtype;
        }

        public String getID() {
            return this.id;
        }

        public void setID(String id) {
            this.id = id;
        }

        public String getAmount()
        {
            return this.amount;
        }

        public void setAmount(String jumlah) {
            this.amount = jumlah;
        }

        public String getMerchantTransactionID() {
            return this.merchantTransactionID;
        }

        public void setMerchantTransactionID(String merchantID) {
            this.merchantTransactionID = merchantID;
        }

        public CLTransactionNameEnum getTransactionNameEnum() {
            return this.transactionNameEnum;
        }

        public void setTransactionNameEnum(CLTransactionNameEnum cltnm) {
            this.transactionNameEnum = cltnm;
        }
    }
}

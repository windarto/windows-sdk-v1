﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public abstract class CLBaseModel
    {
        protected CLResponse errorResponse(JSONServiceDTO dto) {
            CLResponse clBaseResponse = new CLResponse();
            clBaseResponse.setMessage(dto.getError().getMessage());
            clBaseResponse.setHttpStatusCode(200);
            return clBaseResponse;
        }

        protected CLResponse failureResponse(int errorCode) {
            CLResponse clBaseResponse = new CLResponse();
            clBaseResponse.setHttpStatusCode(errorCode);
            return clBaseResponse;
        }

        protected CLResponse unAuthorizedResponse(int errorCode) {
            CLResponse clBaseResponse = new CLResponse();
            clBaseResponse.setHttpStatusCode(errorCode);
            return clBaseResponse;
        }

        protected int okHttpStatus() {
            return 200;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{ 
    public class CLUser {
        private string userID;
        private string pin;

        public CLUser(string userid, string pin) {
            this.userID = userid;
            this.pin = pin;
        }

        public string getUserID() {
            return this.userID;
        }

        public void setUserID(string userid) {
            this.userID = userid;
        }

        public string getPin()
        {
            return this.pin;
        }

        public void setPin(string pin)
        {
            this.pin = pin;
        }
    }
}
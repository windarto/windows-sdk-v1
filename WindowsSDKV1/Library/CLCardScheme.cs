﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public enum CLCardScheme
    {
        VISA, MASTERCARD, JCB, DINERS, AMEX, MAESTRO, CUP, PRIVATE
    }

    public class CLCardSchemeDetail {
        private CLCardSchemeElement[] ListElement = {
            new CLCardSchemeElement("VISA","VISA","prefix 4 (including related/partner brands: Dankort, Electron, etc.)"),
            new CLCardSchemeElement("MASTER", "MASTERCARD", "prefix 5/2221-2720 (including related/partner brands: Diner's Club US, Maestro, etc.)"),
            new CLCardSchemeElement("JCB", "JCB", "prefix 3528-3589"),
            new CLCardSchemeElement("DINERS", "Diners Club International", "prefix 300-305, 309, 36, 38-39"),
            new CLCardSchemeElement("AMEX", "AMERICAN EXPRESS", "prefix 34, 37"),
            new CLCardSchemeElement("MAESTRO", "MAESTRO", "prefix 50, 56-69"),
            new CLCardSchemeElement("CUP", "CHINA UNIONPAY", "prefix 62"),
            new CLCardSchemeElement("PRIVATE", "PRIVATE LABEL", "Private Label"),
        };

        public CLCardSchemeElement getCardSchemeDetail(CLCardScheme clCardScheme) {
            int index = (int)clCardScheme;
            return ListElement[index];
        }

        public CLCardSchemeElement getCardSchemeByString(String schemeString) {
            int i;
            CLCardSchemeElement clelement = null;
            for (i=0;i<=this.ListElement.Length;i++) {
                if (this.ListElement[i].getAppIdentifier() == schemeString) {
                    clelement = this.ListElement[i];
                }
            }
            return clelement;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    class CLCashPay : CLBasePayment
    {
        private List<String> cashVerModes;
         
        public CLCashPay() : base(TransactionTipe.CASH)
        {
            this.id = "CASH";
        }

        public List<String> getCashVerModes() {
            return this.cashVerModes;
        }

        public void setCashVerModes(List<String> cashvermodes) {
            this.cashVerModes = cashvermodes;
        }
    }
}

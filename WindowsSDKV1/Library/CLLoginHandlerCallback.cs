﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public interface CLLoginHandlerCallback
    {
        void onStartActivattion(CLResponse mobileUpdateURL);

        void onLoginSuccess(CLLoginResponse loginResponse);

        void onLoginError(CLErrorResponse errorResponse);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class JSONTransaction
    {
        private String AIDICC;
        private String amountAuthorized;
        private JSONApplication application;
        private String applicationLabel;
        private String approvalCode;
        private String batchNo;
        private String cardHolderName;
        private String cardType;
        private String invoiceNumber;
        private String itemDescription;
        private String itemImage;
        private String maskedPAN;
        private String RRefNo;
        private String traceNo;
        private String transactionCert;
        private String transactionDate;
        private String transactionID;
        private String merchantTransactionID;
        private int transactionStatus;
        private String TransactionTime;
        private String walkinMID;
        private String merchantName;
        private String address1;
        private String address2;
        private String city;

        private String cashlezAppID;
        private String approvedCurrencyCode;
        private TransactionType paymentType;
        private String acquirerBankName = "Mandiri";
        private String responseCode;
        private String hostResponseCode;
        private String applicationCryptogram;
        private String terminalVerificationResults;
        private String footerReceiptMerchant;
        private String footerReceiptBank;
        private String clientTransactionTimezone;

        private String appBankName;
        private String appBankCode;
        private String appBankRefID;
        private String appLoyaltyName;
        private String appLoyaltyType;
        private String appDiscountAmount;
        private String appPointRedeem;
        private String appStatus;

        public String getAppBankCode() { return this.appBankCode; }

        public void setAppBankCode(String AppBankCode) { this.appBankCode = AppBankCode; }

        public String getAppBankName() { return this.appBankName; }

        public void setAppBankName (String AppBankName) { this.appBankName = AppBankName; }

        public String getAppBankRefID() { return this.appBankRefID; }

        public void setAppBankRefID(String AppBankRefID) { this.appBankRefID = AppBankRefID; }

        public String getAppDiscountAmount() { return this.appDiscountAmount; }

        public void setAppDiscountAmount(String AppDiscountAmount) { this.appDiscountAmount = AppDiscountAmount; }

        public String getAppLoyaltyName() { return this.appLoyaltyName; }

        public void setAppLoyaltyName(String AppLoyaltyName) { this.appLoyaltyName = AppLoyaltyName; }

        public String getAppLoyaltyType() { return this.appLoyaltyType; }

        public void setAppLoyaltyType(String AppLoyaltyType) { this.appLoyaltyType = AppLoyaltyType; }

        public String getAppPointRedeem() { return this.appPointRedeem; }

        public void setAppPointRedeem(String AppPointRedeem) { this.appPointRedeem= AppPointRedeem; }

        public String getAppStatus() { return this.appStatus; }

        public void setAppStatus(String AppStatus) { this.appStatus = AppStatus; }

        public void setFooterReceiptBank(String footerReceiptBank) {
            this.footerReceiptBank = footerReceiptBank; }

        public void setFooterReceiptMerchant(String footerReceiptMerchant) {
            this.footerReceiptMerchant = footerReceiptMerchant; }

        public String getAddress1() {
            return this.address1;
        }

        public void setAddress1(String Address1) {
            this.address1 = Address1;
        }

        public String getAddress2()
        {
            return this.address2;
        }

        public void setAddress2(String Address2)
        {
            this.address2 = Address2;
        }

        public String getCity()
        {
            return this.city;
        }

        public void setCity(String City)
        {
            this.city = City;
        }

        public String getMerchantName()
        {
            return this.merchantName;
        }

        public void setMerchantName(String Name)
        {
            this.merchantName = Name;
        }

        public String getResponseCode()
        {
            return this.responseCode;
        }

        public void setResponseCode(String ResponseCode)
        {
            this.responseCode = ResponseCode;
        }

        public String getAcquirerBankName()
        {
            return this.acquirerBankName;
        }

        public void setAcquirerBankName(String AcquirerBankName)
        {
            this.acquirerBankName= AcquirerBankName;
        }

        public TransactionType getPaymentType()
        {
            return this.paymentType;
        }

        public void setPaymentType(TransactionType PaymentType)
        {
            this.paymentType= PaymentType;
        }

        public String getApprovedCurrencyCode()
        {
            return this.approvedCurrencyCode;
        }

        public void setApprovedCurrencyCode(String ApprovedCurrencyCode)
        {
            this.approvedCurrencyCode = ApprovedCurrencyCode;
        }

        public String getCashlezAppID()
        {
            return this.cashlezAppID;
        }

        public void setCashlezAppID(String AcquirerBankName)
        {
            this.acquirerBankName = AcquirerBankName;
        }

        public String getMerchantTransactionID()
        {
            return this.merchantTransactionID;
        }

        public void setMerchantTransactionID(String MerchantTransID)
        {
            this.merchantTransactionID = MerchantTransID;
        }

        public String getTransactionID()
        {
            return this.transactionID;
        }

        public void setTransactionID(String TransID)
        {
            this.transactionID = TransID;
        }

        public String getInvoiceNumber()
        {
            return this.invoiceNumber;
        }

        public void setInvoiceNumber(String InvoiceNumber)
        {
            this.invoiceNumber= InvoiceNumber;
        }

        public String getBatchNo()
        {
            return this.batchNo;
        }

        public void setBatchNo(String BatchNo)
        {
            this.batchNo = BatchNo;
        }

        public String getApprovalCode()
        {
            return this.approvalCode;
        }

        public void setApprovalCode(String ApprovalCode)
        {
            this.approvalCode = ApprovalCode;
        }

        public String getTraceNo()
        {
            return this.traceNo;
        }

        public void setTraceNo(String TraceNo)
        {
            this.traceNo= TraceNo;
        }

        public String getRREFNo()
        {
            return this.RRefNo;
        }

        public void setRREFNo(String refno)
        {
            this.RRefNo= refno;
        }

        public String getWalkinMID()
        {
            return this.walkinMID;
        }

        public void setWalkinMID(String walkinMID)
        {
            this.walkinMID = walkinMID;
        }

        public int getTransactionStatus()
        {
            return this.transactionStatus;
        }

        public void setTraceNo(int TransStatus)
        {
            this.transactionStatus = TransStatus;
        }

        public String getTransactionTime()
        {
            return this.TransactionTime;
        }

        public void setTransactionTime(String TransTime)
        {
            this.TransactionTime = TransTime;
        }

        public String getTransactionDate()
        {
            return this.transactionDate;
        }

        public void setTransactionDate(String TransDate)
        {
            this.transactionDate = TransDate;
        }

        public String getAmountAuthorized()
        {
            return this.amountAuthorized;
        }

        public void setAmountAuthorized (String amount)
        {
            this.amountAuthorized = amount;
        }

        public String getApplicationLabel()
        {
            return this.applicationLabel;
        }

        public void setApplicationLabel(String appLabel)
        {
            this.applicationLabel = appLabel;
        }

        public String getMaskedPAN()
        {
            return this.maskedPAN;
        }

        public void setMaskedPAN(String maskpan)
        {
            this.maskedPAN= maskpan;
        }

        public String getTransactionCert()
        {
            return this.transactionCert;
        }

        public void setTransactionCert(String transcert)
        {
            this.transactionCert = transcert;
        }

        public String getCardType()
        {
            return this.cardType;
        }

        public void setCardType(String CardType)
        {
            this.cardType = CardType;
        }

        public String getAIDICC()
        {
            return this.AIDICC;
        }

        public void setAIDICC(String aidicc)
        {
            this.AIDICC= aidicc;
        }

        public String getCardHoldername()
        {
            return this.cardHolderName;
        }

        public void setCardHoldername (String cardname)
        {
            this.cardHolderName= cardname;
        }

        public String getItemImage()
        {
            return this.itemImage;
        }

        public void setItemImage(String ItemImage)
        {
            this.itemImage = ItemImage;
        }

        public String getItemDescription()
        {
            return this.itemDescription;
        }

        public void setItemDescription (String itemdesc)
        {
            this.itemDescription = itemdesc;
        }

        public JSONApplication getApplication()
        {
            return this.application;
        }

        public void setApplication (JSONApplication app)
        {
            this.application = app;
        }

        public String getApplicationCryptogram()
        {
            return this.applicationCryptogram;
        }

        public void setApplicationCryptogram(String appcrypt)
        {
            this.applicationCryptogram = appcrypt;
        }

        public String getTerminalVerificationResults()
        {
            return this.terminalVerificationResults;
        }

        public void setTerminalVerificationResults(String termverification)
        {
            this.terminalVerificationResults = termverification;
        }

        public String getFooterReceiptMerchant()
        {
            return this.footerReceiptMerchant;
        }

        public String getFooterReceiptBank()
        {
            return this.footerReceiptBank;
        }

        public String getClientTransactionTimezone()
        {
            return this.clientTransactionTimezone;
        }

        public void setClientTransactionTimezone(String timezone)
        {
            this.clientTransactionTimezone = timezone;
        }

        /*public override string ToString()
        {
            return "JSONApplication {" +
                        "AIDICC : '" + this.AIDICC + "\'" + "," +
                        "Amount Authorized : ";
        }*/
    }
}

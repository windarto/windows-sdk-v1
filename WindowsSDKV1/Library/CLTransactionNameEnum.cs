﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{ 
    public enum CLTransactionNameEnum
    {
        DEFAULT, 
        SALE,
        CARD_VER,
        COMPLETION,
        BILL
    };
    public class CLTransactionNameEnums { 
           private int applicationType;

           public int getApplicationType(){
                return this.applicationType;
            }

            public CLTransactionNameEnums(int applicationType)
            {
                this.applicationType = applicationType;
            }
    }
}
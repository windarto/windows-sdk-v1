﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public interface ICLLoginModel
    {
       void doLogin(String Session, ICLJsonHttpUtil jsonHttpUtil, JSONServiceDTO dto, CLKeyJCE keyJCE);
    }
}

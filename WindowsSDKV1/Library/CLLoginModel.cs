﻿using System.Net;

namespace WindowsSDKV1.Library
{
    class CLLoginModel : CLBaseModel, ICLLoginModel, ICLJsonHttpReceiver<JSONServiceDTO>
    {
        private CLLoginServiceCallback clLoginServiceCallback;
        private CLLoginResponse clLoginResponse;
        private CLJsonHttpResult<JSONServiceDTO> result;

        public CLLoginModel(CLLoginServiceCallback clLoginServiceCallback)
        {
            this.clLoginServiceCallback = clLoginServiceCallback;
        }

        public void doLogin(string Session, ICLJsonHttpUtil jsonHttpUtil, JSONServiceDTO dto, CLKeyJCE keyJCE)
        {
            HttpWebRequest webRequest = CLReusableHttpClient.postHttpInstance("process");
            jsonHttpUtil.postAsObjectAsync(Session, dto, keyJCE, webRequest, this.result);
        }

        public void onJsonHttpResult()
        {
            if (result.getHttpStatusCode() == (int)HttpStatusCode.OK)
            {
                if (result.getContent().getError() != null)
                {
                    CLLoginResponse loginResponse = new CLLoginResponse();
                    loginResponse.setHttpStatusCode((int)HttpStatusCode.OK);
                    int errorCode = result.getContent().getError().getCode();
                    if (errorCode == (int)CLErrorStatus.APPLICATION_MOBILE_USER_IS_NOT_ACTIVE)
                    {
                        clLoginServiceCallback.onLoginServiceStartActivation(result.getContent());
                    }
                    else
                    {
                        clLoginServiceCallback.onLoginServiceError(result.getContent().getError());
                    }
                }
                else
                {
                    CLLoginResponse clLoginResponse = new CLLoginResponse();
                    clLoginResponse.setUserName(result.getContent().getUserID());
                    //clLoginResponse.setMerchant(ConstructMerchant(result));
                    clLoginResponse.setHttpStatusCode((int)HttpStatusCode.OK);
                    clLoginServiceCallback.onLoginServiceSuccess(result.getContent(), clLoginResponse);
                }
            }
            else
            {
                clLoginServiceCallback.onLoginServiceNotOk();
            }
        }

        public CLMerchant ConstructMerchant() {
            return new CLMerchant("1","2","3","4","5");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public interface ICLLoginPresenter
    {
        void doHandshake();

        void doHandshake(String username, String PIN);
    }
}

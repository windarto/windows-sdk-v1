﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsSDKV1.Library
{
    public enum TransactionTipe {
        CASH, CREDIT, DEBIT, ECOMM, DIMOPAY, INSTALLMENT,
        RECURRING, CASHLEZ_LINK, PRE_AUTH, OFFLINE, BILL
    };

    public class TransactionType
    {
        private String value;
        private String[] arrenum = { "CASH", "CREDIT", "DEBIT", "ECOMM", "PAY_BY_QR", "INSTALLMENT",
                                     "RECURRING", "CASHLEZ_LINK", "PRE_AUTH", "OFFLINE", "BILL" };

        public TransactionType(int index) {
            this.value = Enum.GetName(typeof(TransactionTipe), index);
        }

        public TransactionType(String Value) {
            this.value = Value;
        }

        public TransactionTipe getTransactionByString(String value) {
            int index = Array.IndexOf(this.arrenum, value);
            if (index == (int)TransactionTipe.BILL) {
                return TransactionTipe.BILL;
            }
            else if (index == (int)TransactionTipe.OFFLINE)
            {
                return TransactionTipe.OFFLINE;
            }
            else if (index == (int)TransactionTipe.PRE_AUTH)
            {
                return TransactionTipe.PRE_AUTH;
            }
            else if (index == (int)TransactionTipe.CASHLEZ_LINK)
            {
                return TransactionTipe.CASHLEZ_LINK;
            }
            else if (index == (int)TransactionTipe.RECURRING)
            {
                return TransactionTipe.RECURRING;
            }
            else if (index == (int)TransactionTipe.INSTALLMENT)
            {
                return TransactionTipe.INSTALLMENT;
            }
            else if (index == (int)TransactionTipe.DIMOPAY)
            {
                return TransactionTipe.DIMOPAY;
            }
            else if (index == (int)TransactionTipe.ECOMM)
            {
                return TransactionTipe.ECOMM;
            }
            else if (index == (int)TransactionTipe.CREDIT)
            {
                return TransactionTipe.CREDIT;
            }
            else if (index == (int)TransactionTipe.DEBIT)
            {
                return TransactionTipe.DEBIT;
            }
            else 
            {
                return TransactionTipe.CASH;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public enum CLDeviceTypeEnums
    {
        CASH = 0,
        PINPAD = 1,
        INGENICO_ICMP_122 = 2,
        PAX = 3,
        BLUEBIRD = 4,
        BBPOS_WISEPAD2 = 5,
        BBPOS_WISEPAD2_PLUS = 6
    };
    public class CLDeviceTypeEnum{
        private int deviceType;
        public CLDeviceTypeEnum(int devicetype)
        {
            this.deviceType = devicetype;
        }

        public int getDeviceType() {
            return this.deviceType;
        }

        //public string toString()
        //{
            //return "CLDeviceTypeEnum {" + "Device Type = " + this.deviceType + "}";
        //}
    }
}

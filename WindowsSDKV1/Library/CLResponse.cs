﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLResponse
    {
        private String Message;
        private int httpStatusCode;

        public String getMessage() {
            return this.Message;
        }

        public void setMessage(String msg) {
            this.Message = msg;
        }

        public int getHttpStatusCode() {
            return this.httpStatusCode;
        }

        public void setHttpStatusCode(int statuscode) {
            this.httpStatusCode = statuscode;
        }
    }
}

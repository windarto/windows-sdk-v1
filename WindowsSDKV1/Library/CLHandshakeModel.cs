﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace WindowsSDKV1.Library
{
    public class CLHandshakeModel : CLBaseModel,ICLJsonHttpReceiver<String>,ICLHandshakeModel
    {
        private static readonly String TAG = "CLHandShakeModel";
        private CLLoginResponse clloginResponse;
        private CLLoginServiceCallback clloginServiceCallback;
        private CLJsonHttpResult<String> result;

        public CLHandshakeModel(CLLoginServiceCallback clLoginServiceCallback) {
            this.clloginServiceCallback = clLoginServiceCallback;
        }

        public void onJsonHttpResult() {
            if (result.getContent() != null ) {
                Debug.WriteLine("HandShake Result : " + result.getContent());
            }

            if ((result.getHttpStatusCode() == 200) && (result.getContent() != null))
            {
                clloginServiceCallback.onHandshakeSuccess(result.getContent());
            }
            else {
                CLLoginResponse clLoginResponse = new CLLoginResponse();
                clLoginResponse.setHttpStatusCode(result.getHttpStatusCode());
                clloginServiceCallback.onHandshakeFail(result.getHttpStatusCode());
            }
        }

        public void decryptKey(string decryptededAesKey, string aesEncryptedMsg)
        {
            String decryptedMessage = null;
            try
            {
                decryptedMessage = CLEncryptionUtil.doAESDecrypt2(decryptededAesKey, aesEncryptedMsg);
                JSONServiceDTO response = JsonConvert.DeserializeObject<JSONServiceDTO>(decryptedMessage);
                if (response.getPKXML() != null) {
                    RSAParameters serverPublicKey;
                    try {
                        serverPublicKey = CLEncryptionUtil.loadXMLtoRSAPublic(response.getPKXML());
                        clloginServiceCallback.onGetPublicKey(response, serverPublicKey);

                        if (response.getError() != null)
                        {
                            CLLoginResponse clLoginResponse = new CLLoginResponse();
                            clLoginResponse.setHttpStatusCode(200);

                            if (response.getError().getCode() == (int)CLErrorStatus.APPLICATION_EXPIRED)
                            {
                                this.clloginServiceCallback.onLoginServiceError(response.getError());
                            }
                            else if (response.getError().getCode() == (int)CLErrorStatus.APPLICATION_NEW_VERSION) {
                                this.clloginServiceCallback.onLoginServiceError(response.getError());
                            }
                        }
                        else {
                            clloginServiceCallback.doLogin();
                        }
                    }
                    catch(CryptographicException e){
                        Debug.WriteLine(e.Message);
                        clloginServiceCallback.onDecryptError();
                    }
                }
            }
            catch (CryptographicException e) {
                Debug.WriteLine(e.Message);
                clloginServiceCallback.onDecryptError();
            }
        }

        public void doPostEncrypted(ICLJsonHttpUtil jsonHttpUtil, string Encrypted)
        {
            jsonHttpUtil.postAsStringAsync(Encrypted, this.result);
        }
    }
}

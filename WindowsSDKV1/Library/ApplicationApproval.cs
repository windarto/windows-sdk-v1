﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class ApplicationApproval
    {

        private long id;
        private DateTime transactionDate;
        private TaskStatus status;
        private Boolean issuerReferral;
        private Boolean invalidResponse;
        private Boolean approvedOnline;
        private String cardVerificationResult;
        private String authResponseCode;
        private String authCode;
        private String issuerAuthData;
        private String issuerScriptResults;
        private String terminalVerificationResults;
        private String transactionStatusInfo;
        private String terminalType;
        private String applicationEffectiveDate;
        private String terminalIdentifier;
        private String terminalCountryCode;
        private String applicationInterchangeProfile;
        private String applicationTransactionCounter;
        private String applicationUsageControl;
        private String issuerCountryCode;
        private String unpredictableNumber;
        private String applicationCryptogram;
        private String cid;
        private String cvmList;
        private String iFDSerialNumber;
        private String issuerActionCodeDefault;
        private String issuerActionCodeDenial;
        private String issuerActionCodeOnline;
        private String issuerApplicationData;
        private String terminalCapabilities;
        private String pan;
        private String panSequence;
        private String AIDICC;
        private String AIDTerminal;
        private String cardHolderName;
        private String applicationLabel;
        private long amount;
        private long amountOther;
        private long amountAuthorized;
        private String merchantIdentifier;
        private String messageType;
        private String posEntryCode;
        private String merchantCategoryCode;
        private String transactionAmount;
        private String cardExpiry;
        private String traceNo;
        private long transactionRequestNo;
        private String track2Equilavent;
        private long stan;
        private String invoiceNumber;

        public long getId()
        {
            return id;
        }

        public void setId(long id)
        {
            this.id = id;
        }

        public DateTime getTransactionDate()
        {
            return transactionDate;
        }

        public void setTransactionDate(DateTime transactionDate)
        {
            this.transactionDate = transactionDate;
        }

        public TaskStatus getStatus()
        {
            return status;
        }

        public void setStatus(TaskStatus status)
        {
            this.status = status;
        }

        public Boolean getIssuerReferral()
        {
            return issuerReferral;
        }

        public void setIssuerReferral( Boolean issuerReferral)
        {
            this.issuerReferral = issuerReferral;
        }

        public Boolean getInvalidResponse()
        {
            return invalidResponse;
        }

        public void setInvalidResponse( Boolean invalidResponse)
        {
            this.invalidResponse = invalidResponse;
        }

        public Boolean getApprovedOnline()
        {
            return approvedOnline;
        }

        public void setApprovedOnline( Boolean approvedOnline)
        {
            this.approvedOnline = approvedOnline;
        }

        public String getCardVerificationResult()
        {
            return cardVerificationResult;
        }

        public void setCardVerificationResult( String cardVerificationResult)
        {
            this.cardVerificationResult = cardVerificationResult;
        }

        public String getAuthResponseCode()
        {
            return authResponseCode;
        }

        public void setAuthResponseCode( String authResponseCode)
        {
            this.authResponseCode = authResponseCode;
        }

        public  String getAuthCode()
        {
            return authCode;
        }

        public  void setAuthCode( String authCode)
        {
            this.authCode = authCode;
        }

        public  String getIssuerAuthData()
        {
            return issuerAuthData;
        }

        public  void setIssuerAuthData( String issuerAuthData)
        {
            this.issuerAuthData = issuerAuthData;
        }

        public  String getIssuerScriptResults()
        {
            return issuerScriptResults;
        }

        public  void setIssuerScriptResults( String issuerScriptResults)
        {
            this.issuerScriptResults = issuerScriptResults;
        }

        public  String getTerminalVerificationResults()
        {
            return terminalVerificationResults;
        }

        public  void setTerminalVerificationResults( String terminalVerificationResults)
        {
            this.terminalVerificationResults = terminalVerificationResults;
        }

        public  String getTransactionStatusInfo()
        {
            return transactionStatusInfo;
        }

        public  void setTransactionStatusInfo( String transactionStatusInfo)
        {
            this.transactionStatusInfo = transactionStatusInfo;
        }

        public  String getTerminalType()
        {
            return terminalType;
        }

        public  void setTerminalType( String terminalType)
        {
            this.terminalType = terminalType;
        }

        public  String getTerminalIdentifier()
        {
            return terminalIdentifier;
        }

        public  void setTerminalIdentifier( String terminalIdentifier)
        {
            this.terminalIdentifier = terminalIdentifier;
        }

        public  String getTerminalCountryCode()
        {
            return terminalCountryCode;
        }

        public  void setTerminalCountryCode( String terminalCountryCode)
        {
            this.terminalCountryCode = terminalCountryCode;
        }

        public  String getApplicationInterchangeProfile()
        {
            return applicationInterchangeProfile;
        }

        public  void setApplicationInterchangeProfile( String applicationInterchangeProfile)
        {
            this.applicationInterchangeProfile = applicationInterchangeProfile;
        }

        public  String getApplicationTransactionCounter()
        {
            return applicationTransactionCounter;
        }

        public  void setApplicationTransactionCounter( String applicationTransactionCounter)
        {
            this.applicationTransactionCounter = applicationTransactionCounter;
        }

        public  String getApplicationUsageControl()
        {
            return applicationUsageControl;
        }

        public  void setApplicationUsageControl( String applicationUsageControl)
        {
            this.applicationUsageControl = applicationUsageControl;
        }

        public  String getIssuerCountryCode()
        {
            return issuerCountryCode;
        }

        public  void setIssuerCountryCode( String issuerCountryCode)
        {
            this.issuerCountryCode = issuerCountryCode;
        }

        public  String getUnpredictableNumber()
        {
            return unpredictableNumber;
        }

        public  void setUnpredictableNumber( String unpredictableNumber)
        {
            this.unpredictableNumber = unpredictableNumber;
        }

        public  String getApplicationCryptogram()
        {
            return applicationCryptogram;
        }

        public  void setApplicationCryptogram( String applicationCryptogram)
        {
            this.applicationCryptogram = applicationCryptogram;
        }

        public  String getCid()
        {
            return cid;
        }

        public  void setCid( String cid)
        {
            this.cid = cid;
        }

        public  String getCvmList()
        {
            return cvmList;
        }

        public  void setCvmList( String cvmList)
        {
            this.cvmList = cvmList;
        }

        public  String getiFDSerialNumber()
        {
            return iFDSerialNumber;
        }

        public  void setiFDSerialNumber( String iFDSerialNumber)
        {
            this.iFDSerialNumber = iFDSerialNumber;
        }

        public  String getIssuerActionCodeDefault()
        {
            return issuerActionCodeDefault;
        }

        public  void setIssuerActionCodeDefault( String issuerActionCodeDefault)
        {
            this.issuerActionCodeDefault = issuerActionCodeDefault;
        }

        public  String getIssuerActionCodeDenial()
        {
            return issuerActionCodeDenial;
        }

        public  void setIssuerActionCodeDenial( String issuerActionCodeDenial)
        {
            this.issuerActionCodeDenial = issuerActionCodeDenial;
        }

        public  String getIssuerActionCodeOnline()
        {
            return issuerActionCodeOnline;
        }

        public  void setIssuerActionCodeOnline( String issuerActionCodeOnline)
        {
            this.issuerActionCodeOnline = issuerActionCodeOnline;
        }

        public  String getIssuerApplicationData()
        {
            return issuerApplicationData;
        }

        public  void setIssuerApplicationData( String issuerApplicationData)
        {
            this.issuerApplicationData = issuerApplicationData;
        }

        public  String getTerminalCapabilities()
        {
            return terminalCapabilities;
        }

        public  void setTerminalCapabilities( String terminalCapabilities)
        {
            this.terminalCapabilities = terminalCapabilities;
        }

        public  String getPan()
        {
            return pan;
        }

        public  void setPan( String pan)
        {
            this.pan = pan;
        }

        public  String getPanSequence()
        {
            return panSequence;
        }

        public  void setPanSequence( String panSequence)
        {
            this.panSequence = panSequence;
        }

        public  String getAIDICC()
        {
            return AIDICC;
        }

        public  void setAIDICC( String aIDICC)
        {
            AIDICC = aIDICC;
        }

        public  String getAIDTerminal()
        {
            return AIDTerminal;
        }

        public  void setAIDTerminal( String aIDTerminal)
        {
            AIDTerminal = aIDTerminal;
        }

        public  String getCardHolderName()
        {
            return cardHolderName;
        }

        public  void setCardHolderName( String cardHolderName)
        {
            this.cardHolderName = cardHolderName;
        }

        public  String getApplicationLabel()
        {
            return applicationLabel;
        }

        public  void setApplicationLabel( String applicationLabel)
        {
            this.applicationLabel = applicationLabel;
        }

        public long getAmount()
        {
            return amount;
        }

        public  void setAmount( long amount)
        {
            this.amount = amount;
        }

        public  long getAmountOther()
        {
            return amountOther;
        }

        public  void setAmountOther( long amountOther)
        {
            this.amountOther = amountOther;
        }

        public  long getAmountAuthorized()
        {
            return amountAuthorized;
        }

        public  void setAmountAuthorized( long amountAuthorized)
        {
            this.amountAuthorized = amountAuthorized;
        }

        public  String getMerchantIdentifier()
        {
            return merchantIdentifier;
        }

        public  void setMerchantIdentifier( String merchantIdentifier)
        {
            this.merchantIdentifier = merchantIdentifier;
        }

        public  String getMessageType()
        {
            return messageType;
        }

        public  void setMessageType( String messageType)
        {
            this.messageType = messageType;
        }

        public  String getPosEntryCode()
        {
            return posEntryCode;
        }

        public  void setPosEntryCode( String posEntryCode)
        {
            this.posEntryCode = posEntryCode;
        }

        public  String getMerchantCategoryCode()
        {
            return merchantCategoryCode;
        }

        public  void setMerchantCategoryCode( String merchantCategoryCode)
        {
            this.merchantCategoryCode = merchantCategoryCode;
        }

        public  String getTransactionAmount()
        {
            return transactionAmount;
        }

        public  void setTransactionAmount( String transactionAmount)
        {
            this.transactionAmount = transactionAmount;
        }

        public  String getCardExpiry()
        {
            return cardExpiry;
        }

        public  void setCardExpiry( String cardExpiry)
        {
            this.cardExpiry = cardExpiry;
        }

        public  String getTraceNo()
        {
            return traceNo;
        }

        public  void setTraceNo( String traceNo)
        {
            this.traceNo = traceNo;
        }

        public long getTransactionRequestNo()
        {
            return transactionRequestNo;
        }

        public void setTransactionRequestNo( long transactionRequestNo)
        {
            this.transactionRequestNo = transactionRequestNo;
        }

        public String getTrack2Equilavent()
        {
            return track2Equilavent;
        }

        public void setTrack2Equilavent( String track2Equilavent)
        {
            this.track2Equilavent = track2Equilavent;
        }

        public long getStan()
        {
            return stan;
        }

        public void setStan( long stan)
        {
            this.stan = stan;
        }

        /**
         * @return the applicationEffectiveDate
         */
        public String getApplicationEffectiveDate()
        {
            return applicationEffectiveDate;
        }

        /**
         * @param applicationEffectiveDate the applicationEffectiveDate to set
         */
        public void setApplicationEffectiveDate(String applicationEffectiveDate)
        {
            this.applicationEffectiveDate = applicationEffectiveDate;
        }

        /**
         * @return the invoiceNumber
         */
        public String getInvoiceNumber()
        {
            return invoiceNumber;
        }

        /**
         * @param invoiceNumber the invoiceNumber to set
         */
        public void setInvoiceNumber(String invoiceNumber)
        {
            this.invoiceNumber = invoiceNumber;
        }

        
    public override String ToString()
        {
            return "ApplicationApproval{" +
                    "id=" + id +
                    ", transactionDate=" + transactionDate +
                    ", status=" + status +
                    ", issuerReferral=" + issuerReferral +
                    ", invalidResponse=" + invalidResponse +
                    ", approvedOnline=" + approvedOnline +
                    ", cardVerificationResult='" + cardVerificationResult + '\'' +
                    ", authResponseCode='" + authResponseCode + '\'' +
                    ", authCode='" + authCode + '\'' +
                    ", issuerAuthData='" + issuerAuthData + '\'' +
                    ", issuerScriptResults='" + issuerScriptResults + '\'' +
                    ", terminalVerificationResults='" + terminalVerificationResults + '\'' +
                    ", transactionStatusInfo='" + transactionStatusInfo + '\'' +
                    ", terminalType='" + terminalType + '\'' +
                    ", applicationEffectiveDate='" + applicationEffectiveDate + '\'' +
                    ", terminalIdentifier='" + terminalIdentifier + '\'' +
                    ", terminalCountryCode='" + terminalCountryCode + '\'' +
                    ", applicationInterchangeProfile='" + applicationInterchangeProfile + '\'' +
                    ", applicationTransactionCounter='" + applicationTransactionCounter + '\'' +
                    ", applicationUsageControl='" + applicationUsageControl + '\'' +
                    ", issuerCountryCode='" + issuerCountryCode + '\'' +
                    ", unpredictableNumber='" + unpredictableNumber + '\'' +
                    ", applicationCryptogram='" + applicationCryptogram + '\'' +
                    ", cid='" + cid + '\'' +
                    ", cvmList='" + cvmList + '\'' +
                    ", iFDSerialNumber='" + iFDSerialNumber + '\'' +
                    ", issuerActionCodeDefault='" + issuerActionCodeDefault + '\'' +
                    ", issuerActionCodeDenial='" + issuerActionCodeDenial + '\'' +
                    ", issuerActionCodeOnline='" + issuerActionCodeOnline + '\'' +
                    ", issuerApplicationData='" + issuerApplicationData + '\'' +
                    ", terminalCapabilities='" + terminalCapabilities + '\'' +
                    ", pan='" + pan + '\'' +
                    ", panSequence='" + panSequence + '\'' +
                    ", AIDICC='" + AIDICC + '\'' +
                    ", AIDTerminal='" + AIDTerminal + '\'' +
                    ", cardHolderName='" + cardHolderName + '\'' +
                    ", applicationLabel='" + applicationLabel + '\'' +
                    ", amount=" + amount +
                    ", amountOther=" + amountOther +
                    ", amountAuthorized=" + amountAuthorized +
                    ", merchantIdentifier='" + merchantIdentifier + '\'' +
                    ", messageType='" + messageType + '\'' +
                    ", posEntryCode='" + posEntryCode + '\'' +
                    ", merchantCategoryCode='" + merchantCategoryCode + '\'' +
                    ", transactionAmount='" + transactionAmount + '\'' +
                    ", cardExpiry='" + cardExpiry + '\'' +
                    ", traceNo='" + traceNo + '\'' +
                    ", transactionRequestNo=" + transactionRequestNo +
                    ", track2Equilavent='" + track2Equilavent + '\'' +
                    ", stan=" + stan +
                    ", invoiceNumber='" + invoiceNumber + '\'' +
                    '}';
        }
    }
}

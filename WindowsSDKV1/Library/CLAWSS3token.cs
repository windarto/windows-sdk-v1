﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{ 

    public class CLAWSS3token {
        private string accessKey;
        private string secretKey;
        private string sessionToken;
        private string s3BucketUrl;
        private string s3BucketName;
        private string merchantRefNo;
        private string userId;

        public string getAccessKey()
        {
            return this.accessKey;
        }

        public void setAccessKey(string accesskey)
        {
            accessKey = accesskey;
        }

        public string getSecretKey()
        {
            return this.secretKey;
        }

        public void setSecretKey(string secretkey)
        {
            this.secretKey = secretkey;
        }

        public string getsessionToken()
        {
            return this.sessionToken;
        }

        public void getSessionToken(string sessiontoken)
        {
            this.sessionToken = sessiontoken;
        }

        public string getS3BucketUrl()
        {
            return this.s3BucketUrl;
        }
    
        public void setS3BucketUrl(string s3bucketurl)
        {
            this.s3BucketUrl = s3bucketurl;
        }

        public string gets3BucketName(){
            return this.s3BucketName;
        }

        public void setS3BucketName(string s3bucketname)
        {
            this.s3BucketName = s3bucketname;
        }

        public string getMerchantRefNo() {
            return this.merchantRefNo;
        }

        public void setMerchantRefNo(string merchantRefNo) {
            this.merchantRefNo = merchantRefNo;
        }

        public string getUserId() {
            return this.userId;
        }

        public void setUserId(string uid) {
            this.userId = uid;
        }

    }

}
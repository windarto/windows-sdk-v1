﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class JSONApplication
    {
        private String applicationName;
        private String tid;
        private String mid;
        private String bankCode;

        public JSONApplication(string appname, string tid, string mid, string bankcode) {
            this.applicationName = appname;
            this.tid = tid;
            this.mid = mid;
            this.bankCode = bankcode;
        }

        public String getApplicationName() {
            return this.applicationName;
        }
        public void SetApplicationName(String appname) {
            this.applicationName = appname;
        }

        public String getTID() {
            return this.tid;
        }
        public void setTID(string tid) {
            this.tid = tid;
        }

        public String getMID()
        {
            return this.mid;
        }
        public void setMID(string mid)
        {
            this.mid = mid;
        }

        public String getBankCode()
        {
            return this.bankCode;
        }
        public void setBankCode(string bankcode)
        {
            this.bankCode = bankcode;
        }
    }
}

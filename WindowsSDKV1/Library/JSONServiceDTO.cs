﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace WindowsSDKV1.Library
{
    [DataContract]
    public class JSONServiceDTO
    {
        public static readonly String TYPE_APDU = "APDU";
        public static readonly String TYPE_APP_SELECTION = "APPSELECTION";
        public static readonly String TYPE_APP_RESULT = "APP_RESULT";
        public static readonly String TYPE_TRANSACTION_REQUEST_ID = "TRANSACTION_REQUEST_ID";
        public static readonly int TRANSACTION_TYPE_GOODS = 0;
        public static readonly int TRANSACTION_TYPE_SERVICES = 1;
        public static readonly String SERVICE_CONTRACT_VERSION = "1.5.0";
        private String userDevice;
        private String billID;
        private bool hideLocation;
        private TransactionType transactionType;
        private String salesReversalTrxID;
        private String voidReversalTrxID;
        private String serviceName;
        private JSONServiceError error;
        private String language;
        [DataMember]private String device;
        private String readerSerialNo;
        private String sessionKey;
        private String MD5Hash;
        [DataMember]private String pkXML;
        private String mobileUpdateURL;
        [DataMember]private String versionNo;
        private String platform;
        private String UserID;
        private String userPIN;
        private String activationCode;
        private List<JSONApplication> applicationList;
        private List<JSONParameter> parameterList;
        private CLBankSetting bankSetting;

        private int pagingNo;
        private int itemsPerPage;
        private bool hasMore;
        private bool canVoid;
        private bool canSettle;
        private String[] searchString;
        private List<JSONTransaction> transactionList;
        private String totalAmount;
        private int totalTransactionCount;

        private String transactionID;
        private String receiptNumber;
        private JSONTransaction transactionDetail;
        private String invoiceNumber;
        private String userAdmin;

        private ApplicationApproval approval;
        private String protocol;
        private String requestStatus;
        private long transactionRequestID;
        private int trxType;
        private String type;

        private String selectedAID;
        private String tid;
        private String authorisationResponseCode;
        private String applicationInterchangeProfile;
        private String aid;
        private String terminalVerificationResults;
        private String PAN;
        private String PANSeqNo;
        private int amountAuthorizedNumeric;
        private String issueApplicationData;
        private String applicationCryptogram;
        private String cryptogramInformationData;
        private String CVMresults;
        private String applicationTransactionCounter;
        private String unpredictableNumber;
        private String transactionSequenceCounter;
        private String track2Equivalent;
        private String transactionDate;
        private String cardHolderName;
        private String applicationLabel;
        private String transactionStatusInfo;
        private CLVerificationMode verificationMode;
        private CLDeviceTypeEnum deviceTypeEnum;
        private String requestContainer;
        private String applicationVersionTerminal;
        private String applicationUsageControl;
        private String PIN;
        private String userNewPIN;
        private String merchantTransactionID;
        private String qrCodeContent;

        private List<CLBillList> billList;
        private CLBillList billDetail;

        private String authCode;
        private String issuerAuthData;
        private String issuerScripts;
        private String hostResponseCode;

        //private CLHost host;

        private String itemDesc;
        private String itemImage;
        private String signatureImage;

        private String longitude;
        private String altitude;
        private String latitude;

        private String mobileNo;
        private String email;

        private bool rememberEmail;
        private bool rememberMobileNo;

        private String reversalMode;

        private String KSN;
        private String IPEK;
        private String KEK;
        private String APDURQCryptoString;
        private String APDURSCryptoString;

        private String helpMessageContent;
        [DataMember]private String appVersionNo;
        [DataMember]private String platformTypeEnum;

        private String changePwd;
        private String responseContainer;
        private String cashTransactionEnabled;

        private String oAuthToken;
        private String clientID;
        private String clientSecret;
        private String clientTransactionTimezone;
        //private CLPaymentCapability paymentCapability;
        private CLAWSS3token awsS3Token;
        private String installmentCode;

        public String getOauthToken() { return this.oAuthToken; }

        public void setOauthToken(String oatuh) { this.oAuthToken = oatuh; }

        public String getClientID() { return this.clientID; }

        public void setClientID(String clientID) { this.clientID = clientID; }

        public String getClientSecret() { return this.clientSecret; }

        public void setClientSecret(String secret) { this.clientSecret = secret; }

        public void appendParameter(JSONParameter param) {
            if (this.parameterList == null) {
                this.parameterList = new List<JSONParameter>();
            }
            parameterList.Add(param);
        }

        public String getServiceName() { return this.serviceName; }

        public void setServiceName(String ServiceName) { this.serviceName = ServiceName; }

        public JSONServiceError getError() { return this.error; }

        public void setError(JSONServiceError salah) { this.error = salah; }

        public String getPlatform() { return this.platform; }
        
        public String getVersionNo() { return this.versionNo; }
        
        public void setVersionNo(String version) { this.versionNo = version; }
        
        public String getLanguage() { return this.language; }

        public void setLanguage(String lang) { this.language = lang; }
        
        public String getPKXML() { return this.pkXML; }

        public void setPKXML(String PKXML) { this.pkXML = PKXML; }
        
        public String getMobileUpdateURL() { return this.mobileUpdateURL; }

        public void setMobileUpdateURL(String mobileUpdate) { this.mobileUpdateURL = mobileUpdate; }

        public String getDevice() { return this.device; }

        public void setDevice(String device) { this.device = device; }

        public String getReaderSerialNo() { return this.readerSerialNo; }

        public void setReaderSerialNo(String readerserial) { this.readerSerialNo = readerserial; }

        public String getUserID() { return this.UserID; }

        public void setUserID(String UID) { this.UserID = UID; }

        public String getUserPIN() { return this.userPIN; }

        public void setUserPIN(String PIN) { this.userPIN = PIN; }

        public List<JSONApplication> getApplicationList() {
            return this.applicationList;
        }

        public void setApplicationList(List<JSONApplication> applist) {
            this.applicationList = applist;
        }

        public CLBankSetting getBankSetting() { return this.bankSetting; }

        public void setBankSetting(CLBankSetting banks) { this.bankSetting = banks; }

        public String getSessionKey() { return this.sessionKey; }

        public void setSessionKey(String session) { this.sessionKey = session; }

        public List<JSONParameter> getParameterList() { return this.parameterList; }

        public void setParameterList(List<JSONParameter> paramlist) { this.parameterList = paramlist; }

        public int getPagingNo() { return this.pagingNo; }

        public void setPagingNo(int page) { this.pagingNo = page; }

        public bool getCanVoid() { return this.canVoid; }

        public void setCanVoid(bool voidvalue) { this.canVoid = voidvalue; }

        public String[] getSearchString() {
            return this.searchString;
        }

        public void setSearchString(String[] searchvalue) { this.searchString = searchvalue; }

        public List<JSONTransaction> getTransactionList() { return this.transactionList; }

        public void setTransactionList(List<JSONTransaction> translist) {
            this.transactionList = translist;
        }

        public bool getHasMore() { return this.hasMore; }

        public void setHasMore(bool hasMore) { this.hasMore = hasMore; }

        public int getItemsPerPage() { return this.itemsPerPage; }

        public void setItemsPerPage(int items) { this.itemsPerPage = items; }

        public String getTotalAmount() { return this.totalAmount; }

        public void setTotalAmount(String total) { this.totalAmount = total; }

        public int getTotalTransactionCount() { return this.totalTransactionCount; }

        public void setTotalTransactioCount(int hitung) { this.totalTransactionCount = hitung; }

        public String getTransactionID() { return this.transactionID; }

        public void setTransactionID(String transID) { this.transactionID = transID; }

        public String getReceiptNumber() { return this.receiptNumber; }

        public void setReceiptNumber(String receipt) { this.receiptNumber = receipt; }

        public JSONTransaction getTransactionDetail (){ return this.transactionDetail; }

        public void setTransactionDetail(JSONTransaction transdetail) { this.transactionDetail = transdetail; }

        public String getInvoiceNumber() { return this.invoiceNumber; }

        public void setInvoiceNumber(String invoice) { this.invoiceNumber = invoice; }

        public String getItemDesc() { return this.itemDesc; }

        public void setItemDesc(String item) { this.itemDesc = item; }

        public String getItemImage() { return this.itemImage; }

        public void setItemImage(String image) { this.itemImage = image; }

        public String getLongitude() { return this.longitude; }

        public void setLongitude(String longitude) { this.longitude = longitude; }

        public String getLatitude () { return this.latitude; }

        public void setLatitude(String latitude) { this.latitude = latitude; }

        public String getMobileNo() { return this.mobileNo; }

        public void setMobileNo(String hp) { this.mobileNo = hp; }

        public String getEmail() { return this.email; }

        public void setEmail(String surat) { this.email = surat; }

        public String getSignatureImage() { return this.signatureImage; }

        public void setSignatureImage(String Signature) { this.signatureImage = Signature; }

        public String getMD5Hash() { return this.MD5Hash; }

        public void setMD5Hash(String md5) { this.MD5Hash = md5; }

        public bool getRememberEmail() { return this.rememberEmail; }

        public void setRememberEmail(bool ingatemail) { this.rememberEmail = ingatemail; }

        public bool getRememberMobileNo() { return this.rememberMobileNo; }

        public void setRememberMobileNo(bool ingatphone) { this.rememberMobileNo = ingatphone; }

        public String getReversalMode() { return this.reversalMode; }

        public void setReversalMode(String reversecode) { this.reversalMode = reversecode; }

        public String getSalesReversalTrxId() { return this.salesReversalTrxID; }

        public void setSalesReversalTrxId(String salesreverse) { this.salesReversalTrxID = salesreverse; }

        public String getVoidReversalTrxId() { return this.voidReversalTrxID; }

        public void setVoidReversalTrxId(String voidreverse) { this.voidReversalTrxID = voidreverse; }

        public ApplicationApproval getApproval() { return this.approval; }

        public void setApproval(ApplicationApproval apapprove) { this.approval = apapprove; }

        public String getProtocol() { return this.protocol; }

        public void setProtocol(String prot) { this.protocol = prot; }

        public String getRequestStatus() { return this.requestStatus; }

        public void setRequestStatus(String request) { this.requestStatus = request; }

        public String getSelectedAID() { return this.selectedAID; }

        public void setSelectedAID(String selectAID) { this.selectedAID = selectAID; }

        public long getTransactionRequestID() { return this.transactionRequestID; }

        public void setTransactionRequestID(long transrequest) { this.transactionRequestID = transrequest; }

        public int getTrxType() { return this.trxType; }

        public void setTrxType(int transtype) { this.trxType = transtype; }

        public String getRequestContainer() { return this.requestContainer; }

        public void setRequestContainer(String reqcon) { this.requestContainer = reqcon; }

        public String getType() { return this.type; }

        public void setType(String tipe) { this.type = tipe; }

        public bool getCanSettle() { return this.canSettle; }

        public void setCanSettle(bool settle) { this.canSettle = settle; }

        public String getKSN() { return this.KSN; }

        public void setKSN(String ksn) { this.KSN = ksn; }

        public String getIPEK() { return this.IPEK; }

        public void setIPEK(String ipek) { this.IPEK = ipek; }

        public String getKEK() { return this.KEK; }

        public void setKEK(String kek) { this.KEK = kek; }

        public String getAPDURQCryptoString() { return this.APDURQCryptoString; }

        public void setAPDURQCryptoString(String apdurq) { this.APDURQCryptoString = apdurq; }

        public String getAPDURSCryptoString() { return this.APDURSCryptoString; }

        public void setAPDURSCryptoString(String apdurs) { this.APDURSCryptoString = apdurs; }

        public String getAltitude() { return this.altitude; }

        public void setAltitude(String alt) { this.altitude = alt; }

        public String getTID() { return this.tid; }

        public void setTID(String Tid) { this.tid = Tid; }

        public String getAuthorizationResponseCode() { return this.authorisationResponseCode; }

        public void setAuthorizationResponseCode(String authresponse) {
            this.authorisationResponseCode = authresponse;
        }

        public String getApplicationInterchangeProfile() {
            return this.applicationInterchangeProfile;
        }

        public void setApplicationInterchangeProfile(String appinterchange) {
            this.applicationInterchangeProfile = appinterchange;
        }

        public String getAID() { return this.aid; }

        public void setAID(String Aid) { this.aid = Aid; }

        public String getTerminalVerificationResults()
        {
            return terminalVerificationResults;
        }

        public void setTerminalVerificationResults(
                String terminalVerificationResults)
        {
            this.terminalVerificationResults = terminalVerificationResults;
        }

        public int getAmountAuthorisedNumeric()
        {
            return this.amountAuthorizedNumeric;
        }

        public void setAmountAuthorisedNumeric(int amountAuthorisedNumeric)
        {
            this.amountAuthorizedNumeric = amountAuthorisedNumeric;
        }

        public String getIssuerApplicationData()
        {
            return this.issueApplicationData;
        }

        public void setIssuerApplicationData(String issuerApplicationData)
        {
            this.issueApplicationData= issuerApplicationData;
        }

        public String getApplicationCryptogram()
        {
            return applicationCryptogram;
        }

        public void setApplicationCryptogram(String applicationCryptogram)
        {
            this.applicationCryptogram = applicationCryptogram;
        }

        public String getCryptogramInformationData()
        {
            return cryptogramInformationData;
        }

        public void setCryptogramInformationData(String cryptogramInformationData)
        {
            this.cryptogramInformationData = cryptogramInformationData;
        }
    

        public String getApplicationTransactionCounter()
        {
            return applicationTransactionCounter;
        }

        public void setApplicationTransactionCounter(
                String applicationTransactionCounter)
        {
            this.applicationTransactionCounter = applicationTransactionCounter;
        }

        public String getUnpredictableNumber()
        {
            return unpredictableNumber;
        }

        public void setUnpredictableNumber(String unpredictableNumber)
        {
            this.unpredictableNumber = unpredictableNumber;
        }

        public String getTransactionSequenceCounter()
        {
            return transactionSequenceCounter;
        }

        public void setTransactionSequenceCounter(String transactionSequenceCounter)
        {
            this.transactionSequenceCounter = transactionSequenceCounter;
        }

        /*public String getTrack2Equilavent()
        {
            return this.track2Equilavent;
        }

        public void setTrack2Equilavent(String track2Equilavent)
        {
            this.track2Equilavent = track2Equilavent;
        }

        public CLHost getHost()
        {
            return host;
        }

        public void setHost(CLHost host)
        {
            this.host = host;
        }*/

        public String getTransactionDate()
        {
            return transactionDate;
        }

        public void setTransactionDate(String transactionDate)
        {
            this.transactionDate = transactionDate;
        }

        public String getCardHolderName()
        {
            return cardHolderName;
        }

        public void setCardHolderName(String cardHolderName)
        {
            this.cardHolderName = cardHolderName;
        }

        public String getApplicationLabel()
        {
            return applicationLabel;
        }

        public void setApplicationLabel(String applicationLabel)
        {
            this.applicationLabel = applicationLabel;
        }

        public String getTransactionStatusInfo()
        {
            return transactionStatusInfo;
        }

        public void setTransactionStatusInfo(String transactionStatusInfo)
        {
            this.transactionStatusInfo = transactionStatusInfo;
        }

        public String getIssuerScripts()
        {
            return issuerScripts;
        }

        public void setIssuerScripts(String issuerScripts)
        {
            this.issuerScripts = issuerScripts;
        }

        public String getAuthCode()
        {
            return authCode;
        }

        public void setAuthCode(String authCode)
        {
            this.authCode = authCode;
        }

        public String getIssuerAuthData()
        {
            return issuerAuthData;
        }

        public void setIssuerAuthData(String issuerAuthData)
        {
            this.issuerAuthData = issuerAuthData;
        }

        public String getPAN()
        {
            return PAN;
        }

        public void setPAN(String pAN)
        {
            PAN = pAN;
        }

        public String getPANSeqNo()
        {
            return PANSeqNo;
        }

        public void setPANSeqNo(String pANSeqNo)
        {
            PANSeqNo = pANSeqNo;
        }

        public CLVerificationMode getVerificationMode()
        {
            return verificationMode;
        }

        public void setVerificationMode(CLVerificationMode verificationMode)
        {
            this.verificationMode = verificationMode;
        }

        public CLDeviceTypeEnum getDeviceTypeEnum()
        {
            return deviceTypeEnum;
        }

        public void setDeviceTypeEnum(CLDeviceTypeEnum deviceTypeEnum)
        {
            this.deviceTypeEnum = deviceTypeEnum;
        }

        public String getApplicationVersionTerminal()
        {
            return applicationVersionTerminal;
        }

        public void setApplicationVersionTerminal(String applicationVersionTerminal)
        {
            this.applicationVersionTerminal = applicationVersionTerminal;
        }

        public String getApplicationUsageControl()
        {
            return applicationUsageControl;
        }

        public void setApplicationUsageControl(String applicationUsageControl)
        {
            this.applicationUsageControl = applicationUsageControl;
        }

        public String getPIN()
        {
            return PIN;
        }

        public void setPIN(String pIN)
        {
            PIN = pIN;
        }

        public TransactionType getTransactionType()
        {
            return transactionType;
        }

        public void setTransactionType(TransactionType transactionType)
        {
            this.transactionType = transactionType;
        }

        public String getActivationCode()
        {
            return activationCode;
        }

        public void setActivationCode(String activationCode)
        {
            this.activationCode = activationCode;
        }

        public String getHelpMessageContent()
        {
            return helpMessageContent;
        }

        public void setHelpMessageContent(String helpMessageContent)
        {
            this.helpMessageContent = helpMessageContent;
        }

        public String getHostResponseCode()
        {
            return hostResponseCode;
        }

        public void setHostResponseCode(String hostResponseCode)
        {
            this.hostResponseCode = hostResponseCode;
        }

        public String getUserNewPIN()
        {
            return userNewPIN;
        }

        public void setUserNewPIN(String userNewPIN)
        {
            this.userNewPIN = userNewPIN;
        }

        public void setUserDevice(String userDevice)
        {
            this.userDevice = userDevice;
        }

        public String getUserDevice()
        {
            return userDevice;
        }

        public String getAppVersionNo()
        {
            return appVersionNo;
        }

        public void setAppVersionNo(String appVersionNo)
        {
            this.appVersionNo = appVersionNo;
        }
        
        public String getPlatformTypeEnum()
        {
            return this.platformTypeEnum;
        }

        public void setPlatformTypeEnum(PlatformTypeEnum platforms)
        {
            this.platformTypeEnum = platforms.getValue();
        }

        public String getUserAdmin()
        {
            return userAdmin;
        }

        public void setUserAdmin(String userAdmin)
        {
            this.userAdmin = userAdmin;
        }

        public void setChangePwd(String changePwd)
        {
            this.changePwd = changePwd;
        }

        public String getChangePwd()
        {
            return changePwd;
        }

        public String getResponseContainer()
        {
            return responseContainer;
        }

        public void setResponseContainer(String responseContainer)
        {
            this.responseContainer = responseContainer;
        }

        public List<CLBillList> getBillList()
        {
            return billList;
        }

        public CLBillList getBillDetail()
        {
            return billDetail;
        }

        public void setBillID(String billID)
        {
            this.billID = billID;
        }

        public String getCashTransactionEnabled()
        {
            return cashTransactionEnabled;
        }

        public String getBillID()
        {
            return billID;
        }

        public void setMerchantTransactionID(String merchantTransactionID)
        {
            this.merchantTransactionID = merchantTransactionID;
        }

        public String getMerchantTransactionID()
        {
            return merchantTransactionID;
        }

        public Boolean getHideLocation()
        {
            return hideLocation;
        }

        public String getQrCodeContent()
        {
            return qrCodeContent;
        }

        public void setQrCodeContent(String qrCodeContent)
        {
            this.qrCodeContent = qrCodeContent;
        }

        public void setHideLocation(Boolean hideLocation)
        {
            this.hideLocation = hideLocation;
        }

        public String getClientTransactionTimeZone()
        {
            return this.clientTransactionTimezone;
        }

        public void setClientTransactionTimeZone(String clientTransactionTimeZone)
        {
            this.clientTransactionTimezone = clientTransactionTimeZone;
        }

        /*public CLPaymentCapability getPaymentCapability()
        {
            return paymentCapability;
        }

        public void setPaymentCapability(CLPaymentCapability paymentCapability)
        {
            this.paymentCapability = paymentCapability;
        }*/

        public CLAWSS3token getAwss3Token()
        {
            return awsS3Token;
        }

        public void setAwss3Token(CLAWSS3token awss3Token)
        {
            this.awsS3Token = awss3Token;
        }

        public String getInstallmentCode()
        {
            return installmentCode;
        }

        public void setInstallmentCode(String installmentCode)
        {
            this.installmentCode = installmentCode;
        }

        public override String ToString()
        {
            String temp = "JSON Data:{ \nPublic Key  : '" + this.getPKXML() + "'\n" +
                                        "App Version : '" + this.getAppVersionNo() +"'\n"+
                                        "Version No  : '" +this.getVersionNo() +"'\n"+
                                      "}";
            return temp;
        }

    }
}

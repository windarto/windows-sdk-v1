﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public interface ICLJsonHttpUtil
    {
        void postAsStringAsync(String Encrypted, CLJsonHttpResult<String> result);

        void postAsObjectAsync(string session, JSONServiceDTO dto, CLKeyJCE keyJCE, HttpWebRequest webRequest, CLJsonHttpResult<JSONServiceDTO> resultout);
    }
}

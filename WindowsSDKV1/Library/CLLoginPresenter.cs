﻿using System;
using System.Security.Cryptography;
using System.Diagnostics;
using Newtonsoft.Json;

namespace WindowsSDKV1.Library
{
    public class CLLoginPresenter : CLBasePresenter, ICLLoginPresenter,CLLoginServiceCallback
    {
        private CLLoginHandlerCallback loginHandlerCallback;
        private CLLoginModel loginModel;
        protected CLHandshakeModel handshakeModel;
        private CLKeyJCE keyJCE;
        protected CLUser user;

        public void doHandshake()
        {
            JSONServiceDTO dto = getHandshakeDTO();
            String encryptedPK = getEncryptedPK(dto);

            this.handshakeModel = new CLHandshakeModel(this);
            handshakeModel.doPostEncrypted(jsonHttpUtil, encryptedPK);
        }

        public void doHandshake(string username, string PIN)
        {
            this.user = CreateUser(username, PIN);
            doHandshake();
        }

        private JSONServiceDTO getHandshakeDTO() {
            RSACryptoServiceProvider handshakekey = new RSACryptoServiceProvider();
            handshakekey = CLEncryptionUtil.genNewRSAKeyPair();
            CLKeyJCE keyJCE = new CLKeyJCE();
            keyJCE.setKeyPair(handshakekey);
            keyJCE.setClientPrivatePublicKey(handshakekey.ExportParameters(true));
            keyJCE.setRSAPublicKey(handshakekey.ExportParameters(false));

            JSONServiceDTO dto = new JSONServiceDTO();
            dto.setPKXML(CLEncryptionUtil.toPKXML(handshakekey.ToXmlString(false)));
            PlatformTypeEnum platenum = new PlatformTypeEnum("WIN_DESKTOP");
            dto.setPlatformTypeEnum(platenum);
            dto.setAppVersionNo("1.5.0.0");
            dto.setVersionNo(JSONServiceDTO.SERVICE_CONTRACT_VERSION);
            dto.setDevice("Windarto");

            return dto;
        }

        private String getEncryptedPK(JSONServiceDTO dto) {
            String encryptedPK = "";
            try
            {
                encryptedPK = CLEncryptionUtil.doAESEncrypt2(JsonConvert.SerializeObject(dto), "0000000000000000000000000000000000000000000000000000000000000000");
            }
            catch (CryptographicException e){
                Debug.WriteLine(e.Message);
            }
            return encryptedPK;
        }

        private CLUser CreateUser(String Username, String PIN) {
            CLUser pengguna = new CLUser(Username.ToLower(),PIN);
            return pengguna;    
        }

        public void onHandshakeSuccess(String result)
        {
            String[] param = result.Split('|');
            String aesKeyEncryptWithPublicKey = param[0];
            String aesEncryptedMsg = param[1];
            try
            {
                String decryptedAesKey = CLEncryptionUtil.decryptRSA(aesKeyEncryptWithPublicKey,keyJCE);
               
                //Decrypt key with new key the AES Encrypted Message
            }
            catch (CryptographicException e)
            {
                Debug.WriteLine(e.Message);
                CLErrorResponse errorHandshake = new CLErrorResponse();
                errorHandshake.setErrorCode((int)CLErrorStatus.HANDSHAKE_FAILED);
                CLErrorStatusValue valueError = new CLErrorStatusValue();
                errorHandshake.setErrorHostMessage(valueError.getMsgfromErrorCode((int)CLErrorStatus.HANDSHAKE_FAILED));
                loginHandlerCallback.onLoginError(errorHandshake);
            }
        }

        public void onHandshakeFail(int httpStatusCode)
        {
            CLErrorResponse errorHandshake = new CLErrorResponse();
            errorHandshake.setErrorCode((int)CLErrorStatus.HANDSHAKE_FAILED);
            CLErrorStatusValue valueError = new CLErrorStatusValue();
            errorHandshake.setErrorHostMessage(valueError.getMsgfromErrorCode((int)CLErrorStatus.HANDSHAKE_FAILED));
            errorHandshake.setHttpStatusCode(httpStatusCode);

            loginHandlerCallback.onLoginError(errorHandshake);
        }   

        public void onDecryptError()
        {
            CLErrorResponse errorHandshake = new CLErrorResponse();
            errorHandshake.setErrorCode((int)CLErrorStatus.DECRYPT_PROCESS_FAILED);
            CLErrorStatusValue valueError = new CLErrorStatusValue();
            errorHandshake.setErrorMessage(valueError.getMsgfromErrorCode((int)CLErrorStatus.DECRYPT_PROCESS_FAILED));

            loginHandlerCallback.onLoginError(errorHandshake);
        }

        public void onLoginServiceStartActivation(JSONServiceDTO content)
        {
            throw new NotImplementedException();
        }

        public void onLoginServiceSuccess(JSONServiceDTO result, CLLoginResponse clLoginResponse)
        {
            throw new NotImplementedException();
        }

        public void onLoginServiceRequestToken(CLJsonHttpResult<JSONServiceDTO> result)
        {
            throw new NotImplementedException();
        }

        public void onLoginServiceNotOk()
        {
            CLErrorResponse errorHandshake = new CLErrorResponse();
            errorHandshake.setErrorCode((int)CLErrorStatus.ERROR_CONNECTION_TIMED_OUT);
            CLErrorStatusValue valueError = new CLErrorStatusValue();
            errorHandshake.setErrorMessage(valueError.getMsgfromErrorCode((int)CLErrorStatus.ERROR_CONNECTION_TIMED_OUT));

            loginHandlerCallback.onLoginError(errorHandshake);
        }

        public void onGetPublicKey(JSONServiceDTO response, RSAParameters serverPublicKey)
        {
            this.keyJCE.setRSAPublicKey(serverPublicKey);
            this.keyJCE.setClientID(response.getClientID());
            this.keyJCE.setSecretKey(response.getClientSecret());
        }

        public void doLogin()
        {
            throw new NotImplementedException();
        }

        public void onLoginServiceError(JSONServiceError clErrorResponse)
        {
            throw new NotImplementedException();
        }
    }
}

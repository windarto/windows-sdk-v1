﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLJsonHttpResult<T>
    {
        private T content;
        private int httpStatusCode = 200;

        public T getContent() {
            return this.content;
        }

        public void setContent(T content) {
            this.content = content;
        }

        public int getHttpStatusCode() {
            return this.httpStatusCode;
        }
        public void setHttpStatusCode(int httpcode) {
            this.httpStatusCode = httpcode;
        }
    }
}

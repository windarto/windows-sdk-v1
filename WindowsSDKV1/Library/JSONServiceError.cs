﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class JSONServiceError
    {
        private int code;
        private String message;
        private String hostResponseMessage = "";
        private String hostErrorMessage = "";
        private String responseContainer;

        public JSONServiceError(int errorcode, String message) {
            this.code = errorcode;
            this.message = message;
        }

        public JSONServiceError(int errorcode, String message, 
                                String hostmessage, String hosterror) {
            this.code = errorcode;
            this.message = message;
            this.hostResponseMessage = hostmessage;
            this.hostErrorMessage = hosterror;
        }

        public JSONServiceError(int errorcode, String message,
                        String hostmessage, String hosterror, String respContainer)
        {
            this.code = errorcode;
            this.message = message;
            this.hostResponseMessage = hostmessage;
            this.hostErrorMessage = hosterror;
            this.responseContainer = respContainer;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }

        public String getHostResponseMessage()
        {
            return this.hostResponseMessage;
        }

        public String getHostErrorMessage() {
            return this.hostErrorMessage;
        }

        public String getResponseContainer() {
            return this.responseContainer;
        }

        public void setResponseContainer(string respContainer) {
            this.responseContainer = respContainer;
        }

    }
}

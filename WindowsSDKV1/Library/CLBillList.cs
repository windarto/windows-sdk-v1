﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLBillList
    {
        private long amount;
        private int amountExtra;
        private String merchantTxid;
        private String description;
        private String currencyCode;
        private String productPicture;
        private String billID;
        private BillStatus billStatus;
        private String createdDate;
        private String createdTime;
        private String clientTransactionTimeZone;

        public CLBillList()
        {

        }

        public CLBillList(String merchantTxid, long amount, String currencyCode) : base()
        {
            this.merchantTxid = merchantTxid;
            this.amount = amount;
            this.currencyCode = currencyCode;
        }

        public enum BillStatus
        {
            CREATED,
            PAID,
            FAILED,
            REMOVED
        }

        public void setAmount(long amount)
        {
            this.amount = amount;
        }

        public long getAmount()
        {
            return amount;
        }

        public void setAmountExtra(int amountExtra)
        {
            this.amountExtra = amountExtra;
        }

        public int getAmountExtra()
        {
            return amountExtra;
        }

        public void setMerchantTxid(String merchantTxid)
        {
            this.merchantTxid = merchantTxid;
        }

        public String getMerchantTxid()
        {
            return merchantTxid;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }

        public void setCurrencyCode(String currencyCode)
        {
            this.currencyCode = currencyCode;
        }

        public String getCurrencyCode()
        {
            return currencyCode;
        }

        public void setProductPicture(String productPicture)
        {
            this.productPicture = productPicture;
        }

        public String getProductPicture()
        {
            return productPicture;
        }

        public void setBillID(String billID)
        {
            this.billID = billID;
        }

        public String getBillID()
        {
            return billID;
        }

        public BillStatus getBillStatus()
        {
            return billStatus;
        }

        public void setBillStatus(BillStatus billStatus)
        {
            this.billStatus = billStatus;
        }

        public void setCreatedDate(String createdDate)
        {
            this.createdDate = createdDate;
        }

        public String getCreatedDate()
        {
            return createdDate;
        }

        public String getClientTransactionTimeZone()
        {
            return clientTransactionTimeZone;
        }

        public void setClientTransactionTimeZone(String clientTransactionTimeZone)
        {
            this.clientTransactionTimeZone = clientTransactionTimeZone;
        }

        public String getCreatedTime()
        {
            return createdTime;
        }

        public void setCreatedTime(String createdTime)
        {
            this.createdTime = createdTime;
        }

        
    public override String ToString()
        {
            return "CLBillList{" +
                    "amount=" + amount +
                    ", amountExtra=" + amountExtra +
                    ", merchantTxid='" + merchantTxid + '\'' +
                    ", description='" + description + '\'' +
                    ", currencyCode='" + currencyCode + '\'' +
                    ", productPicture='" + productPicture + '\'' +
                    ", billID='" + billID + '\'' +
                    ", billStatus=" + billStatus +
                    ", createdDate='" + createdDate +
                    ", clientTransactionTimeZone='" + clientTransactionTimeZone +
                    ", createdTime='" + createdTime +
                    '\'' + '}';
        }
    }
}

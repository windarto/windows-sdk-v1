﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Windows;


namespace WindowsSDKV1.Library
{
    class CLEncryptionUtil
    {
        private static string AES_ENCODED_IV = "cashlezzzpayment";
        private static string AES_ENCODED_IV_HEX = CLMathUtil.byteArraytoString(Encoding.UTF8.GetBytes(AES_ENCODED_IV));
        private static int RSA_BIT = 768;

        public static RSACryptoServiceProvider genNewRSAKeyPair()
        {
            RSACryptoServiceProvider rsapublic = new RSACryptoServiceProvider(RSA_BIT);
            return rsapublic;
        }

        public static String decryptRSA(String encrypted, CLKeyJCE keyJCE) {
            String encryptednowrap = encrypted.Replace(@"\n", "");
            byte[] baseEncrypt = Convert.FromBase64String(encryptednowrap);
            byte[] decryptedtext = new byte[0];

            try
            {
                RSACryptoServiceProvider decrypt = new RSACryptoServiceProvider();
                decrypt.ImportParameters(keyJCE.getClientPrivatePublicKey());
                decryptedtext = decrypt.Decrypt(baseEncrypt, false);
                return Convert.ToBase64String(decryptedtext);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace.ToString());
                return e.StackTrace.ToString();
            }
        }

        public static String doAESEncrypt2(String strDataToEncrypt, String Randomkeyhex)
        {
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] keyBytes = CLMathUtil.stringToByteArray(Randomkeyhex);
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(AES_ENCODED_IV);
            byte[] valueBytes = System.Text.Encoding.UTF8.GetBytes(strDataToEncrypt);
            byte[] encrypted;

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

            aes.BlockSize = 128;
            aes.IV = initVectorBytes;
            aes.Key = keyBytes;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(strDataToEncrypt);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
            return System.Convert.ToBase64String(encrypted);
        }

        public static String doAESDecrypt2(String randomKeyBase64, String encrypted)
        {
            String randomkeybase64nowrap = randomKeyBase64.Replace(@"\n", "");
            byte[] keyBytes = Convert.FromBase64String(randomkeybase64nowrap);

            String ivBase64beforewrap = Convert.ToBase64String(Encoding.UTF8.GetBytes(AES_ENCODED_IV));
            String ivBase64 = ivBase64beforewrap.Replace(@"\n", "");
            byte[] ivBase64Bytes = Convert.FromBase64String(ivBase64);

            byte[] encryptedBytes = Convert.FromBase64String(encrypted);

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.IV = ivBase64Bytes;
            aes.Key = keyBytes;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            String plaintext = null;
            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using (MemoryStream msDecrypt = new MemoryStream(encryptedBytes))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

        public static String toPKXML(String publicKey) {
            return publicKey.Replace(@"RSAKeyValue", "RSAPublicKey");
        }

        public static RSAParameters loadXMLtoRSAPublic(String pkXML) {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(pkXML);
            return rsa.ExportParameters(false);
        }
    }
}

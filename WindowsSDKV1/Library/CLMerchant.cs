﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsSDKV1.Library
{
    public class CLMerchant
    {
        private String Name;
        private String addressFirst;
        private String addressSecond;
        private String city;
        private String logoUrl;

        public CLMerchant(String Name, String addressFirst, String addressSecond, String City, String logoUrl)
        {
            this.Name = Name;
            this.addressFirst = addressFirst;
            this.addressSecond = addressSecond;
            this.city = City;
            this.logoUrl = logoUrl;
        }

        public String getName() {
            return this.Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getAddressFirst() {
            return this.addressFirst;
        }

        public void setAddressFirst(String address1) {
            this.addressFirst = address1;
        }

        public String getAddressSecond() {
            return this.addressSecond;
        }

        public void setAddressSecond(String address2) {
            this.addressSecond = address2;
        }

        public String getCity() {
            return this.city;
        }

        public void setCity(String kota) {
            this.city = kota;
        }

        public String getLogoUrl() {
            return this.logoUrl;
        }

        public void setLogoUrl(String logo) {
            this.logoUrl = logo;
        }
    }
}
